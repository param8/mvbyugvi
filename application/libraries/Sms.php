<?php

class Sms{

	protected $token;

	function __construct() {
		$this->token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiYTM2OWNjYjE1Yzg0NDgxMDU0NjcxOTIyYWY4MTg1M2Q0MDU5ZmM2ZTliYTBhNmVhN2QzY2M3N2FkNzE4OThlOGY2ZWVkMjBlZjcwMDAxOGEiLCJpYXQiOjE2NjExNjcwNDcuODAxMjE2LCJuYmYiOjE2NjExNjcwNDcuODAxMjE5LCJleHAiOjE2OTI3MDMwNDcuNzkwODY4LCJzdWIiOiI2MSIsInNjb3BlcyI6W119.lyRNOYgly1FJHO2tKM8d5G_XSZkeRYjLoiEZQS8Aiu_GOwQluSee_jXEi1kB_qAQagehmgGoYNryGnzG_oOYDBQJvrYh9BTqJkzSPo8wWxF2gCgG4MumPcGF4bnZw-1CEHUbc7DCfjWKr2shHyG4cWRCjQwhWOrVUtWAUwZjd7UL5b5FJn6deGtnm8uprxo1OC4Eq3JjenABzKczMoq1t-TfpFejoV5qHpEtm2FBGQd_xoAES3jOzoKrxdWC6XtiKjKUvpXqQCqNtcxTwLunLMqjjDE7qXSSlvCsiLHntfGzyz21fyVK2Pfa_oMmhTh_gJqnwU5eDsKBHRZ-UllxYwu5Z1OSMGJAekmFG8OS2VynMeG_RTEtKZ_zhJJsb4pa4vbm_WTa0VY3PmT0JTrECOSqL6VPJdb4az0RVyziEsfjv6UiSgsbUeTagL3ERgFqzJiRyUkzQAgjRWjcTgK-E83FwQh17KlnHfjvcH7VFTxGnwxlNRSh2IFoqp6goETcvafFUN3JYinooqIobc9lm48FYz-kwLHBhDnXLhLm3QHcwWqFxnBE8qZb0J3hzEe-znHcxg6NCdygKcB_ZQYyuiIvC4V3sdd_IbndZ1_t-4rJkm_Fa2x3bAM6vmYCQGQ-h04-IrZ4gY4ZkYXtJHhcOG5q-uzOYk8TUW-pzuYNfrI";
	}

	function sendWhatsappMsg($phone, $msg){
		$arr1=[
	          "mobile"=>$phone,
	          "message"=>$msg,
	          ];
        $msg_data=json_encode($arr1);

        try
        {
        	$curl = curl_init();
	        curl_setopt_array($curl, array(
	          CURLOPT_URL => "https://digiglitz.in/whatsapi/api/send/text",
	          CURLOPT_RETURNTRANSFER => true,
	          CURLOPT_ENCODING => "",
	          CURLOPT_MAXREDIRS => 10,
	          CURLOPT_TIMEOUT => 0,
	          CURLOPT_FOLLOWLOCATION => true,
	          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	          CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS =>$msg_data,
	          CURLOPT_HTTPHEADER => array(
	          "Content-Type: application/json",
	          "Authorization: Bearer {$this->token}"
	          ),
	        ));
	        $response = curl_exec($curl);
	        // print_r($response);
	        $res=json_decode($response);
	        curl_close($curl);
	        // print_r($res);
	        return true;  
        }
        catch(Exception $e)
        {
        	// echo $e;
        	return false;
        }
	}
}


?>