<?php
    $physical_system     =  $this->crud_model->get_type_name_by_id('general_settings','68','value');
    $digital_system      =  $this->crud_model->get_type_name_by_id('general_settings','69','value');
    $status= '';
    $value= '';
    if($physical_system !== 'ok' && $digital_system == 'ok'){
        $status= 'digital';
        $value= 'ok';
    }
    if($physical_system == 'ok' && $digital_system !== 'ok'){
        $status= 'digital';
        $value= NULL;
    }
    if($physical_system !== 'ok' && $digital_system !== 'ok'){
        $status= 'digital';
        $value= '0';
    }
?>
<?php
    foreach($offer_data as $row){
?>
<div class="row">
    <div class="col-md-12">
        <?php
            echo form_open(base_url() . 'admin/offers/update/' . $row['offer_id'], array(
                'class' => 'form-horizontal',
                'method' => 'post',
                'id' => 'offer_edit'
            ));
        ?>
            <!--Panel heading-->

            <div class="panel-body">
                    
                <div class="tab-base">
        
        
                    <!--Tabs Content-->                    
                    <div class="tab-content">

                        <div id="offer_details" class="tab-pane fade active in">
        
                            <div class="form-group btm_border">
                                <h4 class="text-thin text-center"><?php echo translate('edit_offer'); ?></h4>                            
                            </div>

                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('title');?></label>
                                <div class="col-sm-6">
                                    <input type="text" name="title" id="demo-hor-1" value="<?php echo $row['offer_title']; ?>" placeholder="<?php echo translate('title');?>" class="form-control required">
                                </div>
                            </div>
                            
                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-6"><?php echo translate('total_buy_item_count');?></label>
                                <div class="col-sm-6">
                                    <input type="number" name="total_buy_item" id="demo-hor-6" min='1' step='1' max='10' value="<?php echo $row['total_buy_item']; ?>" placeholder="<?php echo translate('buy_item_count');?>" class="form-control required">
                                </div>
                            </div>

                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-6"><?php echo translate('total_get_item');?></label>
                                <div class="col-sm-6">
                                    <input type="number" name="total_get_item" id="demo-hor-6" min='1' step='1' max='10' value="<?php echo $row['total_get_item']; ?>" placeholder="<?php echo translate('get_item_count');?>" class="form-control required">
                                </div>
                            </div>

                            <!-- <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-6"><?php //echo translate('offer_discount_type');?></label>
                                <div class="col-sm-6">
                                    <select class="demo-chosen-select" name="offer_discount_type">
                                        <option value="free_item">Free Item</option>
                                        <option value="percent">Percentage (%)</option>
                                    </select>
                                </div>
                            </div> -->

                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-13"><?php echo translate('description'); ?></label>
                                <div class="col-sm-6">
                                    <textarea rows="9"  class="summernotes" data-height="300" data-name="description"><?php echo $row['offer_details']; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('valid_from');?></label>
                                <div class="col-sm-6">
                                    <input type="datetime-local" name="offer_start_date_time" value="<?php echo date('Y-m-d\TH:i:s', strtotime($row['offer_start_date_time'])); ?>" id="demo-hor-1" class="form-control">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('valid_to');?></label>
                                <div class="col-sm-6">
                                    <input type="datetime-local" name="offer_end_date_time" value="<?php echo date('Y-m-d\TH:i:s', strtotime($row['offer_end_date_time'])); ?>" id="demo-hor-1" class="form-control">
                                </div>
                            </div>

                            <div class="form-group product">
                                <label class="col-sm-4 control-label"><?php echo translate('product');?></label>
                                <div class="col-sm-6">
                                    <?php 
                                        $products =json_decode($row['offer_on_pro']);
                                        $result=array();
                                        foreach($products as $row3){
                                            if($this->crud_model->is_publishable($row3)){
                                                $result[]=$row3;
                                            }
                                        }
                                        $status2= '';
                                        $value2= '';
                                        if($physical_system !== 'ok' && $digital_system == 'ok'){
                                            $status2= 'download';
                                            $value2= 'ok';
                                        }
                                        if($physical_system == 'ok' && $digital_system !== 'ok'){
                                            $status2= 'download';
                                            $value2= NULL;
                                        }
                                        if($physical_system !== 'ok' && $digital_system !== 'ok'){
                                            $status2= 'download';
                                            $value2= '0';
                                        }
                                        echo $this->crud_model->select_html('product','product','title','edit','demo-cs-multiselect',json_encode($result),$status2,$value2);
                                    ?>
                                </div>
                            </div>

                            <div class="form-group btm_border">
                                <label class="col-sm-4 control-label" for="demo-hor-1"><?php echo translate('status');?></label>
                                <div class="col-sm-6">
                                    <select name="status" class="form-control required">
                                        <option value="1" <?=($row['offer_status']=='ok')?'selected':'';?>>Enable</option>
                                        <option value="0" <?=($row['offer_status']=='0')?'selected':'';?>>Disable</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

               
            </div>
            
            <div class="panel-footer">
                <div class="row">
                    <div class="col-md-11">
                        <span class="btn btn-purple btn-labeled fa fa-refresh pro_list_btn pull-right" 
                            onclick="ajax_set_full('edit','<?php echo translate('edit_offer'); ?>','<?php echo translate('successfully_edited!'); ?>','offer_edit','<?php echo $row['offer_id']; ?>') "><?php echo translate('reset');?>
                        </span>
                     </div>
                     <div class="col-md-1">
                        <span class="btn btn-success btn-md btn-labeled fa fa-wrench pull-right enterer" onclick="form_submit('offer_edit','<?php echo translate('successfully_edited!'); ?>');proceed('to_add');" ><?php echo translate('edit');?></span> 
                     </div>
                </div>
            </div>
            
        </form>
    </div>
</div>
<?php } ?>
<script src="<?php echo base_url(); ?>template/back/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js">
</script>

<input type="hidden" id="option_count" value="-1">
<script type="text/javascript">
    window.preview = function (input) {
        if (input.files && input.files[0]) {
            $("#previewImg").html('');
            $(input.files).each(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this);
                reader.onload = function (e) {
                    $("#previewImg").append("<div style='float:left;border:4px solid #303641;padding:5px;margin:5px;'><img height='80' src='" + e.target.result + "'></div>");
                }
            });
        }
    }

     $('.delete-div-wrap .close').on('click', function() { 
        var pid = $(this).closest('.delete-div-wrap').find('img').data('id'); 
        var here = $(this); 
        msg = 'Really want to delete this Image?'; 
        bootbox.confirm(msg, function(result) {
            if (result) { 
                 $.ajax({ 
                    url: base_url+''+user_type+'/'+module+'/dlt_img/'+pid, 
                    cache: false, 
                    success: function(data) { 
                        $.activeitNoty({ 
                            type: 'success', 
                            icon : 'fa fa-check', 
                            message : 'Deleted Successfully', 
                            container : 'floating', 
                            timer : 3000 
                        }); 
                        here.closest('.delete-div-wrap').remove(); 
                    } 
                }); 
            }else{ 
                $.activeitNoty({ 
                    type: 'danger', 
                    icon : 'fa fa-minus', 
                    message : 'Cancelled', 
                    container : 'floating', 
                    timer : 3000 
                }); 
            }; 
          }); 
        });

    function other_forms(){}
    
    function set_summer(){
        $('.summernotes').each(function() {
            var now = $(this);
            var h = now.data('height');
            var n = now.data('name');
            now.closest('div').append('<input type="hidden" class="val" name="'+n+'">');
            now.summernote({
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['view', ['codeview', 'help']],
                ],
                height: h,
                onChange: function() {
                    now.closest('div').find('.val').val(now.code());
                }
            });
            now.closest('div').find('.val').val(now.code());
        });
    }

    function option_count(type){
        var count = $('#option_count').val();
        if(type == 'add'){
            count++;
        }
        if(type == 'reduce'){
            count--;
        }
        $('#option_count').val(count);
    }

    function set_select(){
        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width:'100%'});
    }
    
    $(document).ready(function() {
        set_select();
        set_summer();
        createColorpickers();
    });

    

    function get_sub_res(id){}

    $(".unit").on('keyup',function(){
        $(".unit_set").html($(".unit").val());
    });
    
    function createColorpickers() {
    
        $('.demo2').colorpicker({
            format: 'rgba'
        });
        
    }
    
    $('body').on('click', '.rmo', function(){
        $(this).parent().parent().remove();
    });

    function next_tab(){
        $('.nav-tabs li.active').next().find('a').click();                    
    }
    function previous_tab(){
        $('.nav-tabs li.active').prev().find('a').click();                     
    }
    
    $('body').on('click', '.rmon', function(){
        var co = $(this).closest('.form-group').data('no');
        $(this).parent().parent().remove();
        if($(this).parent().parent().parent().html() == ''){
            $(this).parent().parent().parent().html(''
                +'   <input type="hidden" name="op_set'+co+'[]" value="none" >'
            );
        }
    });

    
    $('body').on('click', '.rms', function(){
        $(this).parent().parent().remove();
    });

      
    $('body').on('click', '.rmc', function(){
        $(this).parent().parent().remove();
    });

    
    function delete_row(e)
    {
        e.parentNode.parentNode.parentNode.removeChild(e.parentNode.parentNode);
    }    
    
    
    $(document).ready(function() {
        $("form").submit(function(e){
            return false;
        });
    });
</script>
<style>
    .btm_border{
        border-bottom: 1px solid #ebebeb;
        padding-bottom: 15px;   
    }
</style>

<!--Bootstrap Tags Input [ OPTIONAL ]-->

