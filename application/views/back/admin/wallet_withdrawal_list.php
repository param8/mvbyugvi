<div class="panel-body" id="demo_s">
    <div style="float: left;">
        <input type="text" name="datefilter" class="form-control" value="<?=($start_date!='' && $end_date!='')?date('m/d/Y', $start_date).' - '.date('m/d/Y', $end_date):''?>" placeholder="Filter by date range" />
    </div>
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" >
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo translate('customer');?></th>
                <th><?php echo translate('amount');?></th>
                <th><?php echo translate('date');?></th>
                <th><?php echo translate('bank_name');?></th>
                <th><?php echo translate('account_holder_name');?></th>
                <th><?php echo translate('account_number');?></th>
                <th><?php echo translate('ifsc_code');?></th>

                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>				
        <tbody >
        <?php
            $i = 0;
            foreach($all_wallet_withdrawal as $row){
                $i++;
        ?>                
        <tr>    
            <td>
                <?= $i; ?>
            </td>            
            <td>
                <?php echo ucfirst($row['username']).' '.$row['surname']; ?>
            </td>
            <td><?php echo currency('','def').' '.$this->cart->format_number($row['withdrawal_amount']); ?></td>
            <td><?php echo $row['withdrawal_date']; ?></td>
            <td><?php echo $row['bank_name']; ?></td>
            <td><?php echo $row['account_holder_name']; ?></td>
            <td><?php echo $row['account_number']; ?></td>
            <td><?php echo $row['ifsc_code']; ?></td>
            <td class="text-right">
                <a onclick="delete_confirm('<?php echo $row['wallet_load_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" class="btn btn-xs btn-danger btn-labeled fa fa-trash" data-toggle="tooltip" 
                    data-original-title="Delete" data-container="body">
                        <?php echo translate('delete');?>
                </a>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>
    <div id="vendr"></div>
    <div id='export-div' style="padding:40px;">
		<h1 id ='export-title' style="display:none;"><?php echo translate('wallet_withdrawal'); ?></h1>
		<table id="export-table" class="table" data-name='wallet_withdrawal' data-orientation='p' data-width='1500' style="display:none;">
				<colgroup>
					<col width="50">
					<col width="150">
					<col width="150">
                    <col width="150">
                    <col width="150">
                    <col width="150">
                    <col width="150">
                    <col width="150">
				</colgroup>
				<thead>
					<tr>
						<th>no</th>
                        <th><?php echo translate('customer');?></th>
                        <th><?php echo translate('amount');?></th>
                        <th><?php echo translate('date');?></th>
                        <th><?php echo translate('bank_name');?></th>
                        <th><?php echo translate('account_holder_name');?></th>
                        <th><?php echo translate('account_number');?></th>
                        <th><?php echo translate('ifsc_code');?></th>
					</tr>
				</thead>



				<tbody >
				<?php
					$i = 0;
	            	foreach($all_wallet_withdrawal as $row){
                        $i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
                    <td><?php echo ucfirst($row['username']).' '.$row['surname']; ?></td>
                    <td><?php echo currency('','def').' '.$this->cart->format_number($row['withdrawal_amount']); ?></td>
                    <td><?php echo $row['withdrawal_date']; ?></td>
                    <td><?php echo $row['bank_name']; ?></td>
                    <td><?php echo $row['account_holder_name']; ?></td>
                    <td><?php echo (string)$row['account_number']; ?></td>
                    <td><?php echo $row['ifsc_code']; ?></td>       	
				</tr>
	            <?php
	            	}
				?>
				</tbody>
		</table>
	</div>
           