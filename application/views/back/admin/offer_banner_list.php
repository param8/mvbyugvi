<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,6" data-show-toggle="true" data-show-columns="false" data-search="true" >
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo translate('heading');?></th>
                <th><?php echo translate('image');?></th>
                <th><?php echo translate('link');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>

        <tbody >
        <?php
            $i=0;
            foreach($all_media as $row){
                $i++;
        ?>
        <tr>
            <td><?=$i;?></td>
            <td><?=$row['media_title'];?></td>
            <td><span id="previewImg" >
                    <img class="img-responsive" width="100" src="<?php echo base_url('uploads/media/'.$row['filename']); ?>" alt="Media_Image" >
                </span>
            </td>
            <td><?=$row['media_link'];?></td>
            <td class="text-right">
                <a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" onclick="ajax_set_full('edit','<?php echo translate('edit_media'); ?>','<?php echo translate('successfully_edited!'); ?>','media_edit','<?php echo $row['media_id']; ?>');proceed('to_list');" data-original-title="Edit" data-container="body"><?php echo translate('edit');?>
                </a>

                <a onclick="delete_confirm('<?php echo $row['media_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" 
                    class="btn btn-danger btn-xs btn-labeled fa fa-trash" data-toggle="tooltip" data-original-title="Delete" data-container="body">
                        <?php echo translate('delete');?>
                </a>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>