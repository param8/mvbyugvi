	<div class="panel-body" id="demo_s">
		<table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,2" data-show-toggle="true" data-show-columns="false" data-search="true" >

			<thead>
				<tr>
					<th><?php echo translate('no');?></th>
					<th><?php echo translate('name');?></th>
          <th><?php echo translate('icon image');?></th>
					<th><?php echo translate('detail image');?></th>
					<th class="text-right"><?php echo translate('options');?></th>
				</tr>
			</thead>
				
			<tbody >
			<?php
				$i = 0;
            	foreach($all_ingredents as $row){
            		$i++;
			?>
			<tr>
				<td><?php echo $i; ?></td>
                <td><?php echo $row['title']; ?></td>
				<td>
                    <?php
						if(file_exists('uploads/ingredents_image/'.$row['icon_img'])){
					?>
					<img class="img-md" src="<?php echo base_url(); ?>uploads/ingredents_image/<?php echo $row['icon_img']; ?>" height="100px" />  
					<?php
						} else {
					?>
					<img class="img-md" src="<?php echo base_url(); ?>uploads/ingredents_image/default.jpg" height="100px" />
					<?php
						}
					?> 
               	</td>
								 <td>
                    <?php
						if(file_exists('uploads/ingredents_image/'.$row['detail_img'])){
					?>
					<img class="img-md" src="<?php echo base_url(); ?>uploads/ingredents_image/<?php echo $row['detail_img']; ?>" height="100px" />  
					<?php
						} else {
					?>
					<img class="img-md" src="<?php echo base_url(); ?>uploads/ingredents_image/default.jpg" height="100px" />
					<?php
						}
					?> 
               	</td>
				<td class="text-right">
					<a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" 
                    	onclick="ajax_modal('edit','<?php echo translate('edit_ingredents'); ?>','<?php echo translate('successfully_edited!'); ?>','ingredents_edit','<?php echo $row['id']; ?>')" 
                        	data-original-title="Edit" data-container="body">
                            	<?php echo translate('edit');?>
                    </a>
					<a onclick="delete_confirm('<?php echo $row['id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" class="btn btn-danger btn-xs btn-labeled fa fa-trash" data-toggle="tooltip" 
                    	data-original-title="Delete" data-container="body">
                        	<?php echo translate('delete');?>
                    </a>
				</td>
			</tr>
            <?php
            	}
			?>
			</tbody>
		</table>
	</div>
           
	<div id='export-div'>
		<h1 style="display:none;"><?php echo translate('ingredents'); ?></h1>
		<table id="export-table" data-name='ingredents' data-orientation='p' style="display:none;">
				<thead>
					<tr>
						<th><?php echo translate('no');?></th>
						<th><?php echo translate('title');?></th>
					</tr>
				</thead>
					
				<tbody >
				<?php
					$i = 0;
	            	foreach($all_ingredents as $row){
	            		$i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $row['title']; ?></td>
				</tr>
	            <?php
	            	}
				?>
				</tbody>
		</table>
	</div>

