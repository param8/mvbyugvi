<?php
	foreach($ingredents_data as $row){
?>
<div class="tab-pane fade active in" id="edit">
	<?php
			echo form_open(base_url() . 'admin/ingredents/update/' . $row['id'], array(
				'class' => 'form-horizontal',
				'method' => 'post',
				'id' => 'ingredents_edit',
				'enctype' => 'multipart/form-data'
			));
		?>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-1">
				<?php echo translate('title');?>
			</label>
			<div class="col-sm-6">
				<input type="text" name="title" value="<?php echo $row['title'];?>" id="demo-hor-1"
					class="form-control required" placeholder="<?php echo translate('title');?>" onkeyup="get_slug(this.value)">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-1">
				Slug
			</label>
			<div class="col-sm-6">
				<input type="text" name="slug" value="<?php echo $row['slug'];?>" id="slug_edit"
					class="form-control required slug" placeholder="Slug">
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-2">
				<?php echo translate('Icone Image'); ?>
			</label>
			<div class="col-sm-6">
				<span class="pull-left btn btn-default btn-file">
					<?php echo translate('select_ingredents_icone_image'); ?>
					<input type="file" name="icon_img" id='imgIcon' accept="image">
				</span>
				<br><br>
				<span id='wrap' class="pull-left">
					<?php
								if(file_exists('uploads/ingredents_image/'.$row['icon_img'])){
							?>
					<img src="<?php echo base_url(); ?>uploads/ingredents_image/<?php echo $row['icon_img']; ?>" width="100%"
						id='blah' />
					<?php
								} else {
							?>
					<img src="<?php echo base_url(); ?>uploads/ingredents_image/default.jpg" width="100%" id='blah' />
					<?php
								}
							?>
				</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-2">
				<?php echo translate('Detail Image'); ?>
			</label>
			<div class="col-sm-6">
				<span class="pull-left btn btn-default btn-file">
					<?php echo translate('select_ingredents_detail_image'); ?>
					<input type="file" name="detail_img" id='imgdetail' accept="image">
				</span>
				<br><br>
				<span id='wrap' class="pull-left">
					<?php
								if(file_exists('uploads/ingredents_image/'.$row['detail_img'])){
							?>
					<img src="<?php echo base_url(); ?>uploads/ingredents_image/<?php echo $row['detail_img']; ?>" width="100%"
						id='blah' />
					<?php
								} else {
							?>
					<img src="<?php echo base_url(); ?>uploads/ingredents_image/default.jpg" width="100%" id='blah' />
					<?php
								}
							?>
				</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="description">
				<?php echo translate('ingredents_description');?>
			</label>
			<div class="col-sm-6">
				<textarea type="text" name="description" id="description" class="form-control required"
					placeholder="<?php echo translate('ingredents_description');?>"><?php echo $row['description'];?>
							</textarea>
			</div>
		</div>
		
	</form>
</div>
<?php
	}
?>

<script>
	$(document).ready(function() {
		$("form").submit(function(e) {
			return false;
		});
	});

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#wrap').hide('fast');
				$('#blah').attr('src', e.target.result);
				$('#wrap').show('fast');
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#imgInp").change(function() {
		readURL(this);
	});
</script>
<script>
	function get_slug(value){
		
		var slug = value.toLowerCase()
    .replace(/[^\w ]+/g, "")
    .replace(/ +/g, "-");
		$('.slug').val(slug);
		

	}
</script>