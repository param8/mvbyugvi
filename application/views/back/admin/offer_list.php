	<div class="panel-body" id="demo_s">
		<table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,4" data-show-toggle="true" data-show-columns="false" data-search="true" >

			<thead>
				<tr>
					<th><?php echo translate('no');?></th>
					<th><?php echo translate('title');?></th>
					<th><?php echo translate('start_date');?></th>
					<th><?php echo translate('end_date');?></th>
					<th><?php echo translate('status');?></th>
					<th class="text-right"><?php echo translate('options');?></th>
				</tr>
			</thead>
				
			<tbody >
			<?php
				$i=0;
            	foreach($all_offers as $row){
            		$i++;
			?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['offer_title']; ?></td>
                    <td><?php echo $row['offer_start_date_time']; ?></td>
                    <td><?php echo $row['offer_end_date_time']; ?></td>
		            <td>
		                <?php if($row['offer_status'] == 'ok'){ ?>
		                	<span class="btn-sm btn-info">Enable</span>
		                <?php }else{?>
		                	<span class="btn-sm btn-danger">Disable</span>
		                <?php } ?>
		            </td>
                    
                    <td class="text-right">
                        <a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" 
		                    onclick="ajax_set_full('edit','<?php echo translate('edit_offer'); ?>','<?php echo translate('successfully_edited!'); ?>','offer_edit','<?php echo $row['offer_id']; ?>');proceed('to_list');" data-original-title="Edit" data-container="body">
		                        <?php echo translate('edit');?>
		                </a>
                        <a onclick="delete_confirm('<?php echo $row['offer_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" 
                            class="btn btn-danger btn-xs btn-labeled fa fa-trash" 
                                data-toggle="tooltip" data-original-title="Delete" 
                                    data-container="body"><?php echo translate('delete');?>
                        </a>
                        
                    </td>
                </tr>
            <?php
            	}
			?>
			</tbody>
		</table>
	</div>

	<div id='export-div' style="padding:40px;">
		<h1 id ='export-title' style="display:none;"><?php echo translate('offers'); ?></h1>
		<table id="export-table" class="table" data-name='offers' data-orientation='p' data-width='1500' style="display:none;">
				<colgroup>
					<col width="50">
					<col width="150">
					<col width="150">
					<col width="150">
					<col width="150">
				</colgroup>
				<thead>
					<tr>
						<th><?php echo translate('no');?></th>
						<th><?php echo translate('title');?></th>
						<th><?php echo translate('start_date');?></th>
						<th><?php echo translate('end_date');?></th>
						<th><?php echo translate('status');?></th>
					</tr>
				</thead>



				<tbody >
				<?php
					$i = 0;
	            	foreach($all_offers as $row){
	            		$i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $row['offer_title']; ?></td>
					<td><?php echo $row['offer_start_date_time']; ?></td>
					<td><?php echo $row['offer_end_date_time']; ?></td>
					<td><?php if($row['offer_status'] == 'ok'){ ?>
		                	<span class="btn-sm btn-info">Enable</span>
		                <?php }else{?>
		                	<span class="btn-sm btn-danger">Disable</span>
		                <?php } ?>
		            </td>            	
				</tr>
	            <?php
	            	}
				?>
				</tbody>
		</table>
	</div>




           