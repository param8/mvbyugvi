<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,6" data-show-toggle="true" data-show-columns="false" data-search="true" >
        <thead>
            <tr>
                <th>#</th>
                <th><?php echo translate('heading');?></th>
                <th><?php echo translate('image');?></th>
                <th><?php echo translate('link');?></th>
                <th><?php echo translate('video_url');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>

        <tbody >
        <?php
            $i=0;
            foreach($all_discover as $row){
                $i++;
        ?>
        <tr>
            <td><?=$i;?></td>
            <td><?=$row['discover_title'];?></td>
            <td><span id="previewImg" >
                    <img class="img-responsive" width="100" src="<?php echo base_url('uploads/discover/'.$row['filename']); ?>" alt="Media_Image" >
                </span>
            </td>
            <td><?=$row['discover_link'];?></td>
            <td><?=$row['discover_video_url'];?></td>
            <td class="text-right">
                <a class="btn btn-success btn-xs btn-labeled fa fa-wrench" data-toggle="tooltip" onclick="ajax_set_full('edit','<?php echo translate('edit_discover_now'); ?>','<?php echo translate('successfully_edited!'); ?>','discover_edit','<?php echo $row['discover_id']; ?>');proceed('to_list');" data-original-title="Edit" data-container="body"><?php echo translate('edit');?>
                </a>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>