<?php
	foreach($category_data as $row){
?>
<div class="tab-pane fade active in" id="edit">
	<?php
			echo form_open(base_url() . 'admin/category/update/' . $row['category_id'], array(
				'class' => 'form-horizontal',
				'method' => 'post',
				'id' => 'category_edit',
				'enctype' => 'multipart/form-data'
			));
		?>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-1">
				<?php echo translate('category_name');?>
			</label>
			<div class="col-sm-6">
				<input type="text" name="category_name" value="<?php echo $row['category_name'];?>" id="demo-hor-1"
					class="form-control required" placeholder="<?php echo translate('category_name');?>" onkeyup="get_slug(this.value)">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-1">
				Slug
			</label>
			<div class="col-sm-6">
				<input type="text" name="slug" value="<?php echo $row['slug'];?>" id="slug_edit"
					class="form-control required slug" placeholder="Slug">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-2"><?php echo translate('category_banner');?></label>
			<div class="col-sm-6">
				<span class="pull-left btn btn-default btn-file">
					<?php echo translate('select_category_banner');?>
					<input type="file" name="img" id='imgInp' accept="image">
				</span>
				<br><br>
				<span id='wrap' class="pull-left">
					<?php
								if(file_exists('uploads/category_image/'.$row['banner'])){
							?>
					<img src="<?php echo base_url(); ?>uploads/category_image/<?php echo $row['banner']; ?>" width="100%"
						id='blah' />
					<?php
								} else {
							?>
					<img src="<?php echo base_url(); ?>uploads/category_image/default.jpg" width="100%" id='blah' />
					<?php
								}
							?>
				</span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-3">
				<?php echo translate('category_description');?>
			</label>
			<div class="col-sm-6">
				<textarea type="text" name="category_description" id="demo-hor-3" class="form-control required"
					placeholder="<?php echo translate('category_description');?>"><?php echo $row['description'];?>
							</textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="category_description">
				<?php echo 'Meta Keyword'; ?>
			</label>
			<div class="col-sm-6">
				<textarea type="text" name="meta_Keyword" id="meta_Keyword" class="form-control"
					placeholder="<?php echo 'Enter Meta Keyword'; ?>"><?php echo $row['meta_keyword'];?>
					</textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="category_description">
				<?php echo 'Meta Title'; ?>
			</label>
			<div class="col-sm-6">
				<textarea type="text" name="meta_title" id="meta_title" class="form-control"
					placeholder="<?php echo 'Enter Meta Title'; ?>"><?php echo $row['meta_title'];?>
					</textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-4 control-label" for="category_description">
				<?php echo 'Meta Description'; ?>
			</label>
			<div class="col-sm-6">
				<textarea type="text" name="meta_description" id="meta_description" class="form-control"
					placeholder="<?php echo 'Enter Meta Description'; ?>"><?php echo $row['meta_description'];?>
					</textarea>
			</div>
		</div>
	</div>
	</form>
</div>
<?php
	}
?>

<script>
	$(document).ready(function() {
		$("form").submit(function(e) {
			return false;
		});
	});

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#wrap').hide('fast');
				$('#blah').attr('src', e.target.result);
				$('#wrap').show('fast');
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#imgInp").change(function() {
		readURL(this);
	});
</script>
<script>
	function get_slug(value){
		
		var slug = value.toLowerCase()
    .replace(/[^\w ]+/g, "")
    .replace(/ +/g, "-");
		$('.slug').val(slug);
		

	}
</script>