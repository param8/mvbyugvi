<div id="content-container">
  <div id="page-title">
    <h1 class="page-header text-overflow">Ingredents Detail</h1>
  </div>
  <div class="tab-base">
    <div class="panel">
      <div class="panel-body">
        <div class="tab-content">

          <!-- <div class="tab-pane fade active in" 
                    	id="list" style="border:1px solid #ebebeb; border-radius:4px;">
                    </div> -->

          <div class="row">
            <div class="col-md-12">
              <?php
            echo form_open(base_url() . 'admin/ingredents_detail/do_add/1', array(
                'class' => 'form-horizontal',
                'method' => 'post',
                'id' => 'ingredents_detail_add',
				       'enctype' => 'multipart/form-data'
            ));

            //print_r($ingrediants);die;
        ?>
              <!--Panel heading-->



              <div class="form-group btm_border">
                <h4 class="text-thin text-center"><?php echo translate('ingredents_detail'); ?></h4>
              </div>

              <div class="form-group btm_border">
                <label class="col-sm-4 control-label" for="demo-hor-13"><?php echo translate('banner'); ?></label>
                <div class="col-sm-6">
                  <input type="file" name="banner" class="form-control">
                  <input type="hidden" name="old_banner" value="<?=$ingredents_detail['banner']?>">
                </div>
              </div>

              <div class="form-group btm_border">
                <label class="col-sm-4 control-label" for="demo-hor-13"><?php echo translate('description'); ?></label>
                <div class="col-sm-6">
                  <textarea rows="9" class="summernotes" data-height="200" name="description" data-name="description"><?=$ingredents_detail['description']?></textarea>
                </div>
              </div>


              <div class="panel-footer">
                <div class="row">
                  <!-- <div class="col-md-11">
                    <span class="btn btn-purple btn-labeled fa fa-refresh pro_list_btn pull-right "
                      onclick="ajax_set_full('add','<?php echo translate('add_product'); ?>','<?php echo translate('successfully_added!'); ?>','product_add',''); "><?php echo translate('reset');?>
                    </span>
                  </div> -->

                  <div class="col-md-11">
                    <button type="submit" class="btn btn-success btn-md btn-labeled fa fa-upload pull-right enterer"
                      ><?php echo translate('Add');?></button>
                  </div>

                </div>
              </div>

              </form>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!--Summernote [ OPTIONAL ]-->

<script>

function set_summer() {
  $('.summernotes').each(function() {
    var now = $(this);
    var h = now.data('height');
    var n = now.data('name');
    if (now.closest('div').find('.val').length == 0) {
      now.closest('div').append('<input type="hidden" class="val" name="' + n + '">');
    }
    now.summernote({
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['fontname', ['fontname']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['view', ['codeview', 'help']],
      ],
      height: h,
      onChange: function() {
        now.closest('div').find('.val').val(now.code());
      }
    });
    now.closest('div').find('.val').val(now.code());
  });
}

$(document).ready(function() {
  set_summer();
});
</script>