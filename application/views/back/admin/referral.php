<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow" ><?php echo translate('referral_configuration')?></h1>
	</div>
	<div class="tab-base">
		<!--Tabs Content-->
		<div class="panel">
		<!--Panel heading-->
			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane fade active in" id="lista">
						<div class="panel-body" id="demo_s">
							<?php
                                echo form_open(base_url() . 'admin/referral/update/', array(
                                    'class' => 'form-horizontal',
                                    'method' => 'post'
                                ));
                            ?>
                            
		                        <div class="row">
			                        <h3 class="panel-title"><?php echo translate('referral_limit_for_single_user')?></h3>
					                <div class="form-group btm_border">
					                    <div class="col-sm-12">
					                        <input type="text" name="limit" 
                                            	placeholder="<?php echo translate('refer_code_limit')?>" class="form-control required" value="<?=!empty($refer_config['refer_code_limit'])?$refer_config['refer_code_limit']:'';?>">
					                
					                    </div>
					                </div>
	                            	<h3 class="panel-title"><?php echo translate('signUp_earning_amount_for_referral_user');?> (in <?php echo currency('','def'); ?>)</h3>
					                <div class="form-group btm_border">
					                    <div class="col-sm-12">
					                        <input type="text" name="earning_amount" 
                                            	placeholder="<?php echo translate('sign_up_earning_amount_by_refer_code')?>" class="form-control required" value="<?=!empty($refer_config['earn_amount'])?$refer_config['earn_amount']:'';?>">
					                    </div>
					                </div>

					                <h3 class="panel-title"><?php echo translate('signUp_earning_amount_for_new_user'); ?> (in <?php echo currency('','def'); ?>)</h3>
					                <div class="form-group btm_border">
					                    <div class="col-sm-12">
					                        <input type="text" name="earning_amount_new_user" 
                                            	placeholder="<?php echo translate('sign_up_earning_amount_for_new_user_by_refer_code')?>" class="form-control required" value="<?=!empty($refer_config['earn_amount_new_user'])?$refer_config['earn_amount_new_user']:'';?>">
					                    </div>
					                </div>

					                <h3 class="panel-title"><?php echo translate('bonus_for_referral_user_on_each_purchase');?> (in %)</h3>
					                <div class="form-group btm_border">
					                    <div class="col-sm-12">
                                            <input type="number" name="referral_bonus_on_purchase" id="demo-hor-9" min='0' step='.01' placeholder="<?php echo translate('bonus_percentage_on_each_purchase');?>" class="form-control required" value="<?=!empty($refer_config['referral_bonus_on_purchase'])?$refer_config['referral_bonus_on_purchase']:'';?>">
					                    </div>
					                </div>
	                            	
	                            	<h3 class="panel-title"><?php echo translate('minimum_withdrawal_limit');?> (in <?php echo currency('','def'); ?>)</h3>
					                <div class="form-group btm_border">
					                    <div class="col-sm-12">
					                        <input type="number" name="minimum_withdrawal_limit" min='0' step='1'
                                            	placeholder="<?php echo translate('minimum_withdrawal_limit');?>" class="form-control required" value="<?=!empty($refer_config['minimum_withdrawal_limit'])?$refer_config['minimum_withdrawal_limit']:'';?>">
					                    </div>
					                </div>

					                <div class="form-group btm_border">
					                	<div class="col-sm-12">
						                	<span class="btn btn-info submitter"  data-ing='Submitting...' data-msg='Updated Successfuly!'>
												<?php echo translate('submit')?>
		                            		</span>
		                            	</div>
					                </div>

	                            </div>
	                            
	                        </form>
	                    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>