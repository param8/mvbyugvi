<div>
	<?php
echo form_open(base_url() . 'admin/ingredents/do_add/', array(
    'class' => 'form-horizontal',
    'method' => 'post',
    'id' => 'ingredents_add',
    'enctype' => 'multipart/form-data',
));
?>
	<div class="panel-body">

		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-1">
				<?php echo translate('title'); ?>
			</label>
			<div class="col-sm-6">
				<input type="text" name="title" id="demo-hor-1" class="form-control required"
					placeholder="<?php echo translate('title'); ?>" onkeyup="get_slug(this.value)">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-1">
				Slug
			</label>
			<div class="col-sm-6">
				<input type="text" name="slug" id="slug" class="form-control required slug"
					placeholder="Slug">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-2">
				<?php echo translate('Icone Image'); ?>
			</label>
			<div class="col-sm-6">
				<span class="pull-left btn btn-default btn-file">
					<?php echo translate('select_ingredents_icone_image'); ?>
					<input type="file" name="icon_img" id='imgIcon' accept="image">
				</span>
				<br><br>
				<!-- <span id='wrap' class="pull-left">
					<img src="<?php //echo base_url('uploads/ingredents_image/default.jpg'); ?>" width="100%" id='blah'>
				</span> -->
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="demo-hor-2">
				<?php echo translate('Detail Image'); ?>
			</label>
			<div class="col-sm-6">
				<span class="pull-left btn btn-default btn-file">
					<?php echo translate('select_ingredents_detail_image'); ?>
					<input type="file" name="detail_img" id='imgdetail' accept="image">
				</span>
				<br><br>
				<!-- <span id='wrap' class="pull-left">
					<img src="<?php //echo base_url(); ?>uploads/ingredents_image/default.jpg" width="100%" id='blah'>
				</span> -->
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-4 control-label" for="description">
				<?php echo 'Ingredents Description'; ?>
			</label>
			<div class="col-sm-6">
				<textarea type="text" name="description" id="description" class="form-control"
					placeholder="<?php echo 'Enter Ingredents Description'; ?>">
					</textarea>
			</div>
		</div>
		
		
	</div>
	</form>
</div>

<script>
	$(document).ready(function() {
		$("form").submit(function(e) {
			event.preventDefault();
		});
	});

	function readURL(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#wrap').hide('fast');
				$('#blah').attr('src', e.target.result);
				$('#wrap').show('fast');
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("#imgInp").change(function() {
		readURL(this);
	});
</script>
