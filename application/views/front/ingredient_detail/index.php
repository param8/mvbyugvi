<section class="inigredant pb-5">
   
   <div class="container">
    <p class="breadcrumb-title text-upper"><a href="<?=base_url('/')?>">HOME</a> <i class="fa fa-angle-right "></i> INGREDIENT <i class="fa fa-angle-right "></i><?=$ingredent_detail[0]->title?></p>
       <div class="row d-flex align-items-center">
           <div class="col-md-5">
           <div class="side_banner text-center"> <img src="<?=base_url('uploads/ingredents_image/').$ingredent_detail[0]->detail_img?>" alt="banner" class="W100"> </div>
           </div>
           <div class="col-md-7">
               <div class="detail">
                   
                   <h2 class="mt-0"><?=$ingredent_detail[0]->title?></h2>
               </div>
               <div class="banner_detail mt-5">
                   <p><?=$ingredent_detail[0]->description?></p>
               </div>
           </div>
       </div>
   </div>
   <!-- end -->

</section>
