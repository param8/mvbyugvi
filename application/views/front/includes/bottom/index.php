<script>
    var base_url = "<?php echo base_url(); ?>";
</script>
<script src="<?php echo base_url(); ?>template/front/js/ajax_method.js"></script>
<script src="<?php echo base_url(); ?>template/front/js/bootstrap-notify.min.js"></script>
<script src="<?php echo base_url(); ?>template/front/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>template/front/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>template/front/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
<!-- JS Global -->
<script src="<?php echo base_url(); ?>template/front/plugins/superfish/js/superfish.min.js"></script>
<script src="<?php echo base_url(); ?>template/front/plugins/jquery.sticky.min.js"></script>
<script src="<?php echo base_url(); ?>template/front/plugins/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>template/front/plugins/jquery.smoothscroll.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>template/front/plugins/smooth-scrollbar.min.js"></script> -->
<script src="<?php echo base_url(); ?>template/front/plugins/jquery.cookie.js"></script>

<script src="<?php echo base_url(); ?>template/front/plugins/modernizr.custom.js"></script>
<script src="<?php echo base_url(); ?>template/front/modal/js/jquery.active-modals.js"></script>
<script src="<?php echo base_url(); ?>template/front/js/theme.js"></script>
<script src="<?php echo base_url(); ?>template/front/rateit/jquery.rateit.min.js"></script>

<script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script>

<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

<?php
include $asset_page.'.php';
?>


<form id="cart_form_singl">
    <input type="hidden" name="color" value="">
    <input type="hidden" name="qty" value="1">
</form>

<!-- The Modal -->
  <div class="modal fade" id="confirmLocation">
    <div class="modal-dialog  mod_top  modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header m_header">
          <h4 class="modal-title text-center">Choose Your Currency</h4>
        </div>
        <form class="location" id="getUserLocationForm" action="<?=base_url('home');?>" method="post">
        <!-- Modal body -->
        <div class="modal-body text-center">
                <label class="radio-inline"><input type="radio" name="user_location" value="1" checked=""><span>India [<i class="fa fa-inr"></i>]</span></label>
                <label class="radio-inline"><input type="radio" name="user_location" value="2"><span>USA [<i class="fa fa-dollar"></i>]</span></label>
        </div>
        
        <!-- Modal footer -->
        <div class=" pb-4 modal-footer  m-center">
          <button type="button" class="btn btn-danger" id="getUserLocation">Submit</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<?php
if($this->session->userdata('user_selected_currency') == false) {    
?>
<script type="text/javascript">
    $(window).on('load', function() {
        $('#confirmLocation').modal({backdrop: 'static', keyboard: false}, 'show');
    });

    $('#getUserLocation').click(function(){
        $('#confirmLocation').modal('hide');
        $('#getUserLocationForm').submit();
        var user_loc = $('input[name="user_location"]:checked').val();
        
        $.ajax({
            type: "POST",
            data: {currency:user_loc},
            url: '<?php echo base_url(); ?>home/set_currency/'+user_loc,
            success: function(data){
                    $('#confirmLocation').modal('hide');
                    window.location.reload();
                }
            });

    });
</script>
<?php } ?>