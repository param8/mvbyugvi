<?php 
    $contact_phone =  $this->db->get_where('general_settings',array('type' => 'contact_phone'))->row()->value;
    $contact_email =  $this->db->get_where('general_settings',array('type' => 'contact_email'))->row()->value;
?>
<link href="https://fonts.googleapis.com/css2?family=Jost:wght@400;500;700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet"> 
<link href="catalog/view/theme/upbasket3/stylesheet/stylesheet.css" rel="stylesheet">

<script>
                var url = 'https://wati-integration-prod-service.clare.ai/v2/watiWidget.js?87593';
                var s = document.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = url;
                var options = {
                "enabled":true,
                "chatButtonSetting":{
                    "backgroundColor":"#00e785",
                    "ctaText":"Chat with us",
                    "borderRadius":"25",
                    "marginLeft": "20",
                    "marginRight": "0",
                    "marginBottom": "20",
                    "ctaIconWATI":false,
                    "position":"left"
                },
                "brandSetting":{
                    "brandName":"calix",
                    "brandSubTitle":"undefined",
                    "brandImg":"https://www.wati.io/wp-content/uploads/2023/04/Wati-logo.svg",
                    "welcomeText":"Hi there!\nHow can I help you?",
                    "messageText":"Hello, %0A I have a question about {{page_link}}",
                    "backgroundColor":"#00e785",
                    "ctaText":"Chat with us",
                    "borderRadius":"25",
                    "autoShow":false,
                    "phoneNumber":"91<?= $contact_phone?>"
                }
                };
                s.onload = function() {
                    CreateWhatsappChatWidget(options);
                };
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(s, x);
            </script>
<!-- HEADER -->
<header class="header header-logo-left">
    <div class="header-wrapper">
        <div class="new_topbar">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-sm-6">
                        <div class="top_lheader">
                            Get Free Shipping – Free 30 Day Money Back Guarantee
                        </div>
                    </div>
                    <div class="col-md-5  col-sm-6">
                        <div class="top_lhe_l">
                            <!--<span><a href="#">Order Tracking </a></span>-->
                            <!-- <span>-->
                            <!--    <select class="form-control curr">-->
                            <!--        <option>$ Currency</option>-->
                            <!--        <option>€ Euro</option>-->
                            <!--        <option>£ Pound Sterling</option>-->
                            <!--        <option>$ US Dollar</option>-->
                            <!--    </select>-->
                            <!--</span>-->
                            <!--<span class="countrymenu contact-part no-border"> <div id="google_translate_element"></div></span>-->
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="to_p">
        <div class="container">
            <div class="flex-row     align-items-center">
                <div class="flex-col-3 flex-col-sm-6 flex-col-md-6 flex-col-lg-3  t_o_p">
                <span class="  xs-done d-none">
                    <!--<span class="countrymenu contact-part no-border"> <div id="google_translate_element"></div></span>-->
                <span> <i class="fa fa-mobile"></i> <b>Call:</b> <?=$contact_phone?></span>
                <span class="new"> <i class="fa fa-envelope"></i> <?=$contact_email?></span>
                </span>
                    <!-- Logo -->
                    <div class="logo_t ">
                        <?php
                            $home_top_logo = $this->db->get_where('ui_settings',array('type' => 'home_top_logo'))->row()->value;
                        ?>
                        <a href="<?php echo base_url();?>">
                            <img src="<?php echo base_url(); ?>uploads/logo_image/logo_<?php echo $home_top_logo; ?>.png" alt="SuperShop"/>
                        </a>
                    </div>
                    
                    <!-- /Logo -->
                </div>
                <div class="  flex-col-5 sm-none  flex-col-lg-5 flex-col-md-4 mb_done  ">
                    <!-- Header search -->
                    <div class="header-search for_m mt-4 mt-lg-0   ">                            
                        <?php
                            echo form_open(base_url() . 'home/text_search/', array(
                                'method' => 'post',
                                'accept-charset' => "UTF-8"
                            ));
                        ?>
                            <div class="d-flex position-relative">
                                <div class="flex-grow-1">
                                    <input class="form-control" type="text" name="query"  accept-charset="utf-8" placeholder="<?php echo translate('what_are_you_looking_for');?>?"/>
                                </div>
                              
                                <div>
                                    <?php
                                        if ($this->crud_model->get_type_name_by_id('general_settings','58','value') == 'ok') {
                                    ?>
                                    <select
                                        class="selectpicker header-search-select" data-live-search="true" name="type" onchange="header_search_set(this.value);"
                                        data-toggle="tooltip" title="<?php echo translate('select');?>">
                                        <option value="product"><?php echo translate('product');?></option>
                                    </select>
                                </div>
                                <?php
                                    }
                                ?>
                                <button class="shrc__btn"><i class="fa fa-search"></i>Search</button>
                            </div>
                        </form>
                    </div>
                    <!-- /Header search -->
                </div>
            </div>
             <div class="flex-col-4 flex-col-sm-6 flex-col-md-6 flex-col-lg-4  car_m  text-right">
                    <!-- Header shopping cart -->
                    <div class="header-cart new">
                        <div class="cart-wrapper">
                            <a href="<?php echo base_url(); ?>home/compare" class="btn btn-theme-transparent" id="compare_tooltip" data-toggle="tooltip" data-original-title="<?php echo $this->crud_model->compared_num(); ?>" data-placement="right" >
                                <i class="fa fa-heart ex_c"></i>
                                (
                                <span id="compare_num">
                                    <?php echo $this->crud_model->compared_num(); ?>
                                </span> 
                                ) My WishList
                            </a>
                            <a href="#" class="btn btn-theme-transparent" data-toggle="modal" data-target="#popup-cart">
                                <i class="fa fa-shopping-cart ex_c"></i> 
                                <span class="xs_mob"> 
                                    <span class="cart_num"></span> 
                                </span>  
                                <!--<i class="fa fa-angle-down"></i>-->
                                My Cart
                            </a>
                            <!-- Mobile menu toggle button -->
                            <a href="#" class="menu-toggle btn btn-theme-transparent"><i class="fa fa-bars"></i></a>
                            <!-- /Mobile menu toggle button -->
                        </div>
                    </div>
                    <!-- Header shopping cart -->
                </div>
        </div>
        </div>
        </div>
   <div>
        <div class="navigation-wrapper">
        <div class="container">
            <div class="b_header">
            <!-- Navigation -->
            <?php
            	$others_list=$this->uri->segment(3);
			?>
			<div class="serch_left"> 
			 <!-- Header search -->
                    <div class="header-search ">                            
                        <?php
                            echo form_open(base_url() . 'home/text_search/', array(
                                'method' => 'post',
                                'accept-charset' => "UTF-8"
                            ));
                        ?>
                            <div class="d-flex position-relative">
                                <div class="flex-grow-1" style="display:none">
                                    <input class="form-control" type="text" name="query"  accept-charset="utf-8" placeholder="<?php echo translate('what_are_you_looking_for');?>.."/>
                                </div>
                                <div class="pos cat_g"> 
                                <i class="fa fa-bars abs"></i>
                                    <select
                                        class="selectpicker header-search-select cat_select hidden-xs" data-live-search="true" name="category"
                                        data-toggle="tooltip"  onchange="this.form.submit();" title="<?php echo translate('select');?>">
                                        <option value="0"><?php echo translate('all_categories');?></option>
                                        <?php 
                                            $categories = $this->db->get('category')->result_array();
                                            foreach ($categories as $row1) {
                                                if($this->crud_model->if_publishable_category($row1['category_id'])){
                                        ?>
                                        <option value="<?php echo $row1['slug']; ?>"><?php echo $row1['category_name']; ?></option>
                                        <?php 
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div>
                                    <?php
                                        if ($this->crud_model->get_type_name_by_id('general_settings','58','value') == 'ok') {
                                    ?>
                                    <select
                                        class="selectpicker header-search-select" data-live-search="true" name="type" onchange="header_search_set(this.value);"
                                        data-toggle="tooltip" title="<?php echo translate('select');?>">
                                        <option value="product"><?php echo translate('product');?></option>
                                    </select>
                                </div>
                                <?php
                                    }
                                ?>
                                <!--<button class="shrc_btn"><i class="fa fa-search"></i></button>-->
                            </div>
                        </form>
                    </div>
                    <!-- /Header search -->
             </div>
           
            <nav class="navigation closed clearfix">
                <a href="#" class="menu-toggle-close btn"><i class="fa fa-times"></i></a>
                <ul class="nav sf-menu">
                    <?php if($this->db->get_where('ui_settings',array('type'=>'header_homepage_status'))->row()->value == 'yes'){?>
                    <li <?php if($asset_page=='home'){ ?>class="active"<?php } ?>>
                        <a href="<?php echo base_url(); ?>home">
                            <?php echo translate('homepage');?>
                        </a>
                    </li>
                    <?php } ?>
                    
                    <li class="lin_k">
                        <a href="<?php echo base_url(); ?>home/page/about_us" class=" l"> About   </a>
                         
                    </li>
                    
                    <?php    
                    if($this->db->get_where('ui_settings',array('type'=>'header_all_categories_status'))->row()->value == 'yes'){?>    
                    <li class="lin_k <?php if($asset_page=='all_category'){ echo 'active'; } ?>">
                        <a href="<?php echo base_url('home/others_product/latest');?>">
                        All Products
                        </a>
                        
                    </li>
                    <?php } ?>
                    <li class="lin_k">
                        <a href="<?php echo base_url(); ?>home/blog" class=" "> Blog </a>
 
                    </li>
                   
                     <li class="lin_k">
                        <a href="<?php echo base_url(); ?>home/contact" class=" "> Contact </a> 
                        </li>

                     <?php
                    	if ($this->crud_model->get_type_name_by_id('general_settings','58','value') == 'ok' && $this->crud_model->get_type_name_by_id('general_settings','81','value') == 'ok') {
                            if($this->db->get_where('ui_settings',array('type'=>'header_store_locator_status'))->row()->value == 'yes') {
					?>
                    <li <?php if($asset_page=='store_locator'){ ?>class="active"<?php } ?>>
                        <a href="<?php echo base_url(); ?>home/store_locator">
                            <?php echo translate('store_locator');?>
                        </a>
                    </li>
                    <?php
                            }
						}
					?>
                    <?php if($this->db->get_where('ui_settings',array('type'=>'header_contact_status'))->row()->value == 'yes') {?>
                    <li <?php if($asset_page=='contact'){ ?>class="active"<?php } ?>>
                        <!--<a href="<?php //echo base_url(); ?>home/contact">-->
                        <!--    <?php //echo translate('contact');?>-->
                        <!--</a>-->
                    </li>
                   
                    <?php }?>
                </ul>
            </nav>
              </div>
             <div class="logo_m1">
                       <div style="display:none">
                            <?php
                            $home_top_logo = $this->db->get_where('ui_settings',array('type' => 'home_top_logo'))->row()->value;
                        ?>
                       <a href="<?php echo base_url();?>">
                             <img src="<?php echo base_url(); ?>uploads/logo_image/logo_<?php echo $home_top_logo; ?>.png" alt="SuperShop"/>
                         </a>
                       </div>
                       <div class="offer text-end">
                           <ul class="list-unstyled d-flex list-inline mb-0">              
                               <li class="header-right d-inline-block">
                            <img src="<?php echo base_url();?>/template/front/img/star.png" alt="img">
                               </li>
                              <div class="call-right d-inline-block text-left">
                              <a href="https://opencart.dostguru.com/FD04/upbasket_03/index.php?route=product/special">Only This Weekend</a>
                              <span class="userdess">Super Discount</span>
                              </div>
                          </ul>
                       </div>
                    </div>
              
             
            
            
            <!-- /Navigation -->
        </div>
    </div>
   </div>
   
</header>
<!-- /HEADER -->
<?php
if ($this->crud_model->get_type_name_by_id('general_settings','58','value') !== 'ok') {
?>
<style>
.header.header-logo-left .header-search .header-search-select .dropdown-toggle {
    /*right: 40px !important;*/
}
.btn.disabled, .btn[disabled], fieldset[disabled] .btn {
    cursor: not-allowed;
    pointer-events: none;
    filter: alpha(opacity=65);
    -webkit-box-shadow: none;
    box-shadow: none;
    opacity: .65;
}
</style>
<?php
}
?>