<div class="modal_wrap">
    <div class="row get_into" id="login">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php
                echo form_open(base_url() . 'home/profile/wallet/process_withdrawal/', array(
                    'class' => 'form-login',
                    'method' => 'post',
                    'id' => 'wallet_withdrawal'
                ));
            ?>
                <div class="row box_shape" style="box-shadow:none;overflow-wrap: break-word; word-wrap: break-word;">

                    <?php if($action['status'] == 'invalid'){ ?>
                        <div class="title">
                            <?php echo translate('withdrawal_info');?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p>Minimum withdrawal amount should be <?=currency($minimum_limit);?> or more.</p>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <span class="btn btn-theme-sm btn-block btn-theme-dark pull-right" onclick="closeMe();">
                                <?php echo translate('close');?>
                            </span>
                        </div>
                    <?php } else if($action['status'] == 'bank_info_missing') { ?>
                        <div class="title">
                            <?php echo translate('withdrawal_info');?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <p>Please update Bank details from edit profile section for withdrawal.</p>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <span class="btn btn-theme-sm btn-block btn-theme-dark pull-right" onclick="closeMe();">
                                <?php echo translate('close');?>
                            </span>
                        </div>
                    <?php } else if($action['status'] == 'valid') { ?>
                        <div class="title">
                            <?php echo translate('withdrawal_info');?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group" style="overflow-wrap: break-word; word-wrap: break-word;">
                                <p>Are you sure to withdrawal <?=currency($balance);?> from your wallet.</p>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <span class="btn btn-theme-sm btn-block btn-theme-dark pull-right info_add_btn snbtn">
                                <?php echo translate('submit');?>
                            </span>
                        </div>                        
                    <?php } ?>

                </div>
            </form>
        </div>
    </div>

</div>
<style>
.g-icon-bg {
background: #ce3e26;
}
.g-bg {
background: #de4c34;
height: 37px;
margin-left: 41px;
width: 166px;
}
.modal_wrap{
    padding: 20px 0px;
}
.get_into hr {
    border: 1px solid #e8e8e8  !important;
    height: 0px !important;
    background-image: none !important;
}
.box_shape2 {
    padding: 15px;
    border: solid 1px #e9e9e9;
    background-color: #ffffff;
    margin: -25px 20px;
}
</style>