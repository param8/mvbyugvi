<section class="inigredant pb-5">
    <div class="banner"> <img src="<?=base_url('uploads/inigredant/'.$ingredent_data->banner)?>" alt="banner"> </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="bann_er">
                    <ul class="brimcoad">
                        <li>Home</li> / <li><b>Ingredient</b></li>
                    </ul>
                    <h2 class="mt-0">Our All-Natural Ingredients</h2>
                </div>
                
                <div class="banner_detail mt-5">
                    <p><?=$ingredent_data->description?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- end -->

</section>
<!-- end -->
<section class="prod_u_c_t">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="AllIngredient mb-5">
                    <h2>All Ingredients</h2>
                </div>
            </div>
           
            <div class="col-md-12">
            <div class="row ">
            <?php foreach($ingredents as $ingredent){?>
            <div class="col-md-2  all_p">
                <div class="al_l text-center">
                   <a href="<?=base_url('ingredient/').$ingredent->slug?>">
                   <img src="<?=base_url('uploads/ingredents_image/').$ingredent->icon_img?>" alt="">
                    <p><?=$ingredent->title?></p>
                   </a>
                </div>
            </div>
            <!-- end -->
            <?php }?>
        </div>
            </div>
            <div class="col-md-12">
                <p class="text-center mt-4"><button class="more_btn" id="loadMore">Show More</button></p>
            </div>
        </div>

    </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>

<script>

    $(function () {
        $(".all_p").slice(0, 6).addClass('display');
        $("#loadMore").on('click', function (e) {
            // e.preventDefault();
            $(".all_p:hidden").slice(0, 6).addClass('display');
            if ($(".all_p:hidden").length == 0) {
                $("#loadMore").remove();
            } else {
                $('html,body').animate({
                    // scrollTop: $(this).offset().top
                }, 1500);
            }
        });
    });



</script>