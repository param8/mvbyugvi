<?php
    $discus_id = $this->db->get_where('general_settings',array('type'=>'discus_id'))->row()->value;
    $fb_id = $this->db->get_where('general_settings',array('type'=>'fb_comment_api'))->row()->value;
    $comment_type = $this->db->get_where('general_settings',array('type'=>'comment_type'))->row()->value;
    
    function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}
    
?>
<style>
    .review-comment{
        margin: 1em;
        padding: 1em;
    }
</style>
<!-- PAGE -->
<section class="page-section specification" style="padding-top:0">
    <div class="container">
         <div class="modal fade" id="WHOAMI" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                            <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">WHO AM I ?</h4>
                        </div>
                        <div class="modal-body">
                           <?php echo $row['description'];?>
                        </div>
                      </div>
                    </div>
                  </div>
         <div class="topdescription" style="display: none;">
           <h3><strong>WHO AM I ?</strong></h3>
           <?php  echo limit_text($row['description'], 80); ?>
          <button type="button" class="commonbtn" data-toggle="modal" data-target="#WHOAMI">Read More</button>
         </div>
            <?php  $a = $this->crud_model->get_additional_fields($row['product_id']); if(count($a)>0) {?>
           <div class="topdescription">
                <h3><strong>WHAT DO I DO ?</strong></h3>
                 <div class="row">
                     
							<?php
                            $all_ki = $this->crud_model->get_additional_fields($row['product_id']);
                            if(!empty($all_ki)){
                            $i=0;
                            foreach($all_ki as $row1){
                            ?>
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="producctbenefits"> 
                                <img src="<?php echo base_url('uploads/product_benefit_image/'.$row1['image']); ?>">
                                <h4><?php echo $row1['name']; ?></h4>
                                <?php  echo limit_text($row1['value'], 15); ?>
                                <button type="button" class="commonbtn" data-toggle="modal" data-target="#whatdoid<?php echo $i; ?>">Read More</button>
                               </div>
                            </div>
                        <?php  $i++;}?>
                    </div>
                </div>
            <?php }}?>
        
					<?php
                    $all_ki = $this->crud_model->get_additional_fields($row['product_id']);
                    if(!empty($all_ki)){
					$i=0;	
                    foreach($all_ki as $row1){
                    ?>
                  <div class="modal fade" id="whatdoid<?php echo $i; ?>" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                            <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><?php echo $row1['name']; ?></h4>
                        </div>
                        <div class="modal-body">
                          <?php echo $row1['value']; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php $i++; }}?>
        
        <div class="tabs-wrapper content-tabs">
            <ul class="nav nav-tabs">
                <!--<li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>-->
                <li class="active"><a href="#tab2" data-toggle="tab">Feature and Benefits</a></li>
                <li><a href="#tab3" data-toggle="tab">Indication</a></li>
                <li><a href="#tab4" data-toggle="tab">Ingredients</a></li>
                <!--<li><a href="#tab5" data-toggle="tab">Skin Type</a></li>-->
                <!--<li><a href="#tab6" data-toggle="tab">How to Use</a></li>-->
                 <li><a href="#tab7" data-toggle="tab">Videos</a></li>
            </ul>
    
            <div class="tab-content">
                
                <div class="tab-pane fade in " id="tab1" itemprop="description">
                     <?php echo $row['description'];?>
                </div>
                <div class="tab-pane fade in active" id="tab2" itemprop="feature_benefits">
                     <?php echo $row['feature_benefits'];?>
                </div>
                <div class="tab-pane fade" id="tab3" itemprop="indication">
                     <?php echo $row['indication'];?>
                </div>
                <div class="tab-pane fade" id="tab4" itemprop="ingredients_list">
                <div class="row ">
                    <?php foreach($ingredents as $ingredent){?>
                        <div class="col-md-2 all_p display">
                            <div class="al_l text-center">
                            <a href="<?=base_url('ingredient/').$ingredent->slug?>">
                            <img src="<?=base_url('uploads/ingredents_image/').$ingredent->icon_img?>" alt="">
                                <p><?=$ingredent->title?></p>
                            </a>
                            </div>
                           
                        </div>
                        <!-- end -->
                        <?php }?>
                        </div>
                </div>
                
                <!--<div class="tab-pane fade" id="tab5" itemprop="skin_type">-->
                <!--   <div class="ingradint">-->
                <!--          <?php //echo $row['skin_type'];?>-->
                <!--    </div>-->
                <!--</div>-->
                
                <!--<div class="tab-pane fade" id="tab6" itemprop="how_to_use">-->
                <!--   <div class="ingradint">-->
                <!--    <?//=$row['how_to_use']?>-->
                         
                <!--    </div>-->
                <!--</div>-->
                
                <div class="tab-pane fade" id="tab7" itemprop="videos">
                   <div class="row">
                       <?php
                       $video=explode(',',$videoes);
                       
                       if(!empty(array_filter($video))){
                          // echo count($video);
                       foreach($video as $vid){
                       ?>
                          <div class="col-md-4">
                              <iframe height="300" src="<?=$vid;?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                          </div>
                         <?php }}else{
                         echo "<p class='text-center'> Videos Coming Soon...</p>";}?>
                    </div>
                </div>
               
                
                <div class="tab-pane fade" id="tab3" itemprop="description" style="display: none;">
                    <div class="row">
                         <?php
                            $all_wo = $this->crud_model->get_why_calix_herbal($row['product_id']);
							  $i=0;
                                foreach($all_wo as $row3){
                        ?>
                        <div class="col-md-6 col-sm-12 whyorg">
                            <div class="ingradint">
                              <h3><?php echo $row3['name']; ?></h3>           
                                  <div class="row">
                                    <div class="col-md-4 col-sm-12 col-xs-12 padding-r5">
                                         <img src="<?php echo base_url('uploads/product_why_calhb/'.$row3['image']) ?>" class="width-100">
                                    </div>
                                    <div class="col-md-8 col-sm-12  col-xs-12 padding-lr5">
                                        <ul class="first whyorgaglo">                                         
                                         <?php echo $row3['value']; ?>
                                        </ul>
                                        <button type="button" class="commonbtn" data-toggle="modal" data-target="#whyorgaglo<?php echo $i; ?>">Read More</button> 
                                   </div>
                                 </div>
                            </div>
                        </div>
                        <?php $i++;} ?> 
                       <?php
                            $all_wo = $this->crud_model->get_why_calix_herbal($row['product_id']);
							  $i=0;
                             foreach($all_wo as $row3){
                        ?>
                  <div class="modal fade" id="whyorgaglo<?php echo $i; ?>" role="dialog">
                    <div class="modal-dialog">
                      <!-- Modal content-->
                      <div class="modal-content">
                            <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><?php echo $row3['name']; ?></h4>
                        </div>
                        <div class="modal-body">
                          <?php echo $row3['value']; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php $i++; }?>
                       
                   </div>
                </div>  
                
            
            </div>
        </div>
        <?php
            $all_faqs = $this->crud_model->get_all_faqs($row['product_id']);
            if(!empty($all_faqs)){
        ?>

        <div class="topdescription">
            <h3><strong>FAQs</strong></h3>
            <div class="panel-group" id="accordion">
                <?php 
                $i=0;
                foreach($all_faqs as $row4){
                ?>
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $i;?>">
                        <span class="glyphicon glyphicon-menu-right text-success"></span><?php echo $row4['title'];?>
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne<?php echo $i;?>" class="panel-collapse collapse <?=($i==0)?'':'';?>">
                    <div class="panel-body"><?php echo $row4['description'];?></div>
                  </div>
                </div>
                <?php 
                  $i++; }
                ?>
            </div>
        </div><!-- end container -->
        <?php } ?>
        <?php
            //if(!empty($row['disclaimer'])){
        ?>
        <!-- <div class="topdescription">
           <h3><strong>Disclaimer</strong></h3>
          <?php //echo $row['disclaimer'];?>
         </div> -->
        <?php //} ?>
        <?php
            //if(!empty($row['other_information'])){
        ?>
        
            <!-- <div class="panel-group" id="accordion">
            
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        <span class="glyphicon glyphicon-menu-right text-success"></span><b>Additional Information </b>
                      </a>
                    </h3>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse">
                    <div class="panel-body"><?php //echo $row['other_information'];?></div>
                  </div>
                </div>
            </div> -->
       
        <?php //} ?>
         <div class="topdescription" id="ratingreview">
         <?php

                    $rating_form  = empty($user_given_rating) ? true : false;
                    $user_logged_in =  $this->session->userdata('user_login') == "yes" ? true : false;
                    ?>

                    <h3><?= translate('Ratings & Reviews')?></h3>
                    <span <?php if($user_logged_in || !$user_logged_in) { ?> style="display: none" <?php } ?> >
                            <p><?= translate('Log in to add/edit rating')?></p>
                    </span>
                    <span <?php if($user_bought_product || !$user_bought_product) { ?> style="display: none" <?php } ?> >
                            <p><?= translate('You have to buy the product to give a review')?></p>
                    </span>

                    <div <?php if(!($user_logged_in && $user_bought_product)) { ?> style="display: none" <?php } ?>>

                        <span id="my_rating_span" <?php if($rating_form) { ?> style="display: none" <?php } ?>>
                            <b>Me </b> <div id="given_rating_star" class="rateit" data-rateit-value="<?= isset($user_given_rating['rating'])? $user_given_rating['rating']: 0?>" data-rateit-ispreset="true" data-rateit-readonly="true"></div>

                            <div class="review-comment" id="given_rating_comment" <?php if(!(isset($user_given_rating['comment']) && !empty($user_given_rating['comment']))) { ?> style="display: none" <?php } ?> >
                                 <?= isset($user_given_rating['comment'])? $user_given_rating['comment']: ""?>
                             </div>
                            <div class="form-group">
                                <button id="edit_my_rating" class="btn btn-primary"><?= translate('Edit')?></button>
                            </div>
                        </span>

                        <span id="my_rating_edit_span" <?php if(!$rating_form) { ?> style="display: none" <?php } ?>>
                            <input type="hidden" id="rating_editable" value="">
                            <input type="hidden" id="backing_rateit_product_by_user">
                            <input id="set_product_rating" name="set_product_rating" type="hidden"
                                   value="<?= isset($user_given_rating['rating']) ? $user_given_rating['rating'] : 0 ?>">
                            <input id="set_product_id" name="set_product_id" type="hidden"
                                   value="<?= $row['product_id'] ?>">
                            <input id="set_product_type" name="set_product_type" type="hidden"
                                   value="product">
                            <div id="rateit_product_by_user"
                                 data-rateit-resetable="false"
                                 data-rateit-ispreset="true"
                                 data-rateit-value="<?= isset($user_given_rating['rating']) ? $user_given_rating['rating'] : 0 ?>"
                            >
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" name="set_product_rating_comment"
                                          id="set_product_rating_comment"><?= isset($user_given_rating['comment']) ? $user_given_rating['comment'] : "" ?></textarea>
                            </div>
                            <div class="form-group">
                                <button id="submit_my_rating" class="btn btn-primary"><?= translate('Submit')?></button>
                            </div>
                        </span>

                    </div>

                    <div class="panel">
                        <div class="panel-body">
                            <div class="row">
                                <?php if (!empty($user_ratings)) { ?>
                                    <?php foreach ($user_ratings as $ur) { ?>
                                        <?php if ($ur['user_id'] != $this->session->userdata('user_id')) { ?>
                                            <span itemprop="review" itemscope itemtype="http://schema.org/Review">
                                                <b itemprop="author"><?= $ur['username']?></b>
                                                <span itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                                                    <div itemprop="ratingValue"  class="rateit" data-rateit-value="<?=$ur['rating']?>" data-rateit-ispreset="true" data-rateit-readonly="true"></div>
                                                </span>
                                                <?php if (!empty($ur['comment'])) { ?>
                                                    <div class="review-comment" itemprop="name">
                                                        <?= $ur['comment']?>
                                                    </div>
                                                <?php } ?>
                                                <hr>
                                            </span>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                                <?php if (empty($user_ratings)) { ?>
                                    <?='Please be first to Rate.'?>
                                <?php } ?>
                            </div>
                        </div>
                    </div>


					<?php if($comment_type == 'disqus'){ ?>
                    <div id="disqus_thread"></div>
                    <script type="text/javascript">
                        /* * * CONFIGURATION VARIABLES * * */
                        var disqus_shortname = '<?php echo $discus_id; ?>';
                        
                        /* * * DON'T EDIT BELOW THIS LINE * * */
                        (function() {
                            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                        })();
                    </script>
                    <script type="text/javascript">
                        /* * * CONFIGURATION VARIABLES * * */
                            var disqus_shortname = '<?php echo $discus_id; ?>';
                        
                        /* * * DON'T EDIT BELOW THIS LINE * * */
                        (function () {
                            var s = document.createElement('script'); s.async = true;
                            s.type = 'text/javascript';
                            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
                            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
                        }());
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                    <?php
                        }
                        else if($comment_type == 'facebook'){
                    ?>

                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];=;
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=<?php echo $fb_id; ?>";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments" data-href="<?php echo $this->crud_model->product_link($row['product_id']); ?>" data-numposts="5"></div>

                    <?php
                        }
                    ?>
         </div>
        
    </div>
</section>
<!-- /PAGE -->
<style>
@media(max-width: 768px) {
	.specification .nav-tabs>li{
		float: none;
		display: block;
		text-align: center;
	}
}
</style>


<script type="text/javascript">
    $(function () {
        var rateit_product_by_user =  $("#rateit_product_by_user");
        var edit_my_rating =  $("#edit_my_rating");
        var submit_my_rating =  $("#submit_my_rating");
        var my_rating_span =  $("#my_rating_span");
        var my_rating_edit_span =  $("#my_rating_edit_span");

        var set_product_rating =  $("#set_product_rating");
        var set_product_rating_comment =  $("#set_product_rating_comment");
        var set_product_id =  $("#set_product_id");
        var set_product_type =  $("#set_product_type");

        var given_rating_star =  $("#given_rating_star");
        var given_rating_comment =  $("#given_rating_comment");

        rateit_product_by_user.rateit(
            { max: 5, min:0, step: 0.5, backingfld: '#backing_rateit_product_by_user' }
        );
        rateit_product_by_user.bind('rated', function (event, value) {
            set_product_rating.val(value);
        });
        rateit_product_by_user.bind('reset', function () {
            set_product_rating.val(1);
        });
        rateit_product_by_user.bind('over', function (event, value) {
        });

        edit_my_rating.on('click',function (e) {
            my_rating_span.hide();
            my_rating_edit_span.show();
        });

        submit_my_rating.on('click',function (e) {
            var rating = set_product_rating.val();
            var comment = set_product_rating_comment.val();
            var product_id = set_product_id.val();
            var product_type = set_product_type.val();

            ajaxRequest = $.ajax({
                url: "<?= base_url()?>home/ajax_post_user_rating",
                type: "post",
                data:
                    {
                        "rating":rating,
                        "comment":comment,
                        "product_id":product_id,
                        "product_type":product_type,
                    }
            });

            /*  request cab be abort by ajaxRequest.abort() */

            ajaxRequest.done(function (response, textStatus, jqXHR){
                // show successfully for submit message

                given_rating_star.rateit('value', rating);

                given_rating_comment.html(comment);
                if(comment != "" && comment != null){
                    given_rating_comment.show();
                }else{
                    given_rating_comment.hide();
                }

                my_rating_span.show();
                my_rating_edit_span.hide();
            });

            /* On failure of request this function will be called  */
            ajaxRequest.fail(function (){
                alert("error");
                my_rating_span.show();
                my_rating_edit_span.hide();
            });
        });
    })
</script>
<script>
function textcollapse() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
  }
}
</script>
<style>#more {display: none;}</style>