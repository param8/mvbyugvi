<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<form action="<?php echo base_url('home/razorpay_verify');?>" method="POST" id="pay-form" style="display: none;">
  <script
    src="https://checkout.razorpay.com/v1/checkout.js"
    data-key="<?php echo $key;?>"
    data-amount="<?php echo $amount; ?>"
    data-currency="INR"
    data-name="<?php echo $name; ?>"
    data-image="<?php echo $image; ?>"
    data-description="<?php echo $description; ?>"
    data-prefill.name="<?php echo $prefill['name']?>"
    data-prefill.email="<?php echo $prefill['email']?>"
    data-prefill.contact="<?php echo $prefill['contact']?>"
    data-notes.shopping_order_id="<?php echo $notes['merchant_order_id']; ?>"
    data-order_id="<?php echo $order_id;?>"
    <?php if ($display_currency !== 'INR') { ?> data-display_amount="<?php echo $display_amount;?>" <?php } ?>
    <?php if ($display_currency !== 'INR') { ?> data-display_currency="<?php echo $display_currency;?>" <?php } ?>
  >
  </script>
  <!-- Any extra fields to be submitted with the form but not sent to Razorpay -->
  <input type="hidden" name="shopping_order_id" value="<?php echo $notes['merchant_order_id']; ?>">
</form>
<script>

jQuery(function(){
   jQuery('.razorpay-payment-button').click();
});
</script>