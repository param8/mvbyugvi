<?php
echo form_open(base_url() . 'home/cart_finish/go', array(
            'method' => 'post',
            'enctype' => 'multipart/form-data',
            'id' => 'cart_form'
        )
    );
?>
<script src="https://checkout.stripe.com/checkout.js"></script>
<!-- PAGE -->
<section class="page-section color">
    <div class="container box_shadow">

       
       <div class="ord_er" id="ord_er">
            <h3 class="block-title alt">
            <i class="fa fa-angle-down"></i>
            <?php echo translate('1');?>.
            <?php echo translate('orders');?>
        </h3>
        <div class="row orders">

        </div>
       </div>
        <div class="d_hide" id="delivery_address">
        <h3 class="block-title alt">
            <i class="fa fa-angle-down"></i>
            <?php echo translate('2');?>.
            <?php echo translate('delivery_address');?>
        </h3>
        <div action="#" class="form-delivery delivery_address ">
        </div>
        </div>

 <div id="payments" class="d_hide">
        <h3 class="block-title alt">
            <i class="fa fa-angle-down"></i>
            <?php echo translate('3');?>.
            <?php echo translate('payments_options');?>
        </h3>
        <div class="panel-group payments-options " id="accordion " role="tablist" aria-multiselectable="true">
        </div>
          
        
    </div>
</section>
<!-- /PAGE -->
<?php echo form_close()?>
<div class="check-loader" style="display: none; position: fixed; top: 50%;z-index: 9; left: 50%;margin-top: -128px; margin-left: -128px;">
    <img src="<?php echo base_url('template/front/img/check-loader.gif');?>">
    <p>Please wait! we are validating your data.</p>
</div>
<script>



    $(document).ready(function(){
     
      
		var top = Number(200);
		$('.orders').html('<div style="text-align:center;width:100%;height:'+(top*2)+'px; position:relative;top:'+top+'px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>');
        var state = check_login_stat('state');
        state.success(function (data) {
            if(data == 'hypass'){
                load_orders();
            } else {
                signin('guest_checkout');
            }
        });
    });

    function load_orders(){
		var top = Number(200);
		$('.orders').html('<div style="text-align:center;width:100%;height:'+(top*2)+'px; position:relative;top:'+top+'px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>');
        $('.orders').load('<?php echo base_url(); ?>home/cart_checkout/orders');
    }

    function load_address_form(){

 
//   alert(" hide order, show address ");
  
document.getElementById("ord_er").style.display = "none";
document.getElementById("delivery_address").style.display = "block";
		var top = Number(200);
		$('.delivery_address').html('<div style="text-align:center;width:100%;height:'+(top*2)+'px; position:relative;top:'+top+'px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>');

        $('.delivery_address').load('<?php echo base_url(); ?>home/cart_checkout/delivery_address',
            function(){
                var top_off = $('.header').height();
                $('.selectpicker').selectpicker();
                $('html, body').animate({
                    scrollTop: $(".delivery_address").offset().top-(2*top_off)
                }, 1000);
            }
        );
        
          
    }
    // end

    function load_payments(){
        document.getElementById("payments").style.display = "block";
document.getElementById("delivery_address").style.display = "none";
		$('#order_place_btn').removeClass('disabled')
        var okay = 'yes';
        var sel = 'no';
        $('.delivery_address').find('.required').each(function(){
            if($(this).is('select') || $(this).is('input')){
                //alert($(this).val());
                if($(this).val() == ''){
                    okay = 'no';
                    if($(this).is('select')){
                        $(this).closest('.form-group').find('.selectpicker').focus();
                    } else {
                        if(sel == 'no'){
                            $(this).focus();
                        }
                    }

                    //alert(okay);
                    //$(this).css('background','red');
                }
            }
        });
        if(okay == 'yes'){
			var top = Number(200);
			$('.payments-options').html('<div style="text-align:center;width:100%;height:'+(top*2)+'px; position:relative;top:'+top+'px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>');
            $('.payments-options').load('<?php echo base_url(); ?>home/cart_checkout/payments_options',
                function(){
                    var top_off = $('.header').height();
                    $('html, body').animate({
                        scrollTop: $(".payments-options").offset().top-(2*top_off)
                    }, 1000);
                }
            );
        } else {
            var top_off = $('.header').height();
            $('html, body').animate({
                scrollTop: $(".delivery_address").offset().top-(2*top_off)
            }, 1000);
        }
    }

    function radio_check(id){
        $( "#visa" ).prop( "checked", false );
        $( "#mastercardd" ).prop( "checked", false );
        $( "#mastercard" ).prop( "checked", false );
        $( "#bitcoin" ).prop( "checked", false );
        $( "#"+id ).prop( "checked", true );
    }
</script>