<?php 
	$contact_address =  $this->db->get_where('general_settings',array('type' => 'contact_address'))->row()->value;
	$contact_phone =  $this->db->get_where('general_settings',array('type' => 'contact_phone'))->row()->value;
	$contact_email =  $this->db->get_where('general_settings',array('type' => 'contact_email'))->row()->value;
	$contact_website =  $this->db->get_where('general_settings',array('type' => 'contact_website'))->row()->value;
	$contact_about =  $this->db->get_where('general_settings',array('type' => 'contact_about'))->row()->value;
	
	$facebook =  $this->db->get_where('social_links',array('type' => 'facebook'))->row()->value;
	$googleplus =  $this->db->get_where('social_links',array('type' => 'google-plus'))->row()->value;
	$twitter =  $this->db->get_where('social_links',array('type' => 'twitter'))->row()->value;
	$skype =  $this->db->get_where('social_links',array('type' => 'skype'))->row()->value;
	$youtube =  $this->db->get_where('social_links',array('type' => 'youtube'))->row()->value;
	$pinterest =  $this->db->get_where('social_links',array('type' => 'pinterest'))->row()->value;
	
	$footer_text =  $this->db->get_where('general_settings',array('type' => 'footer_text'))->row()->value;
	$footer_category =  json_decode($this->db->get_where('general_settings',array('type' => 'footer_category'))->row()->value);
?>
<footer class="footer1">
	<div class="footer1-widgets">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
					<div class="widget">
					    <h3 class="mb-2 mb-lg-3 text_c fs-lg-34 fs-214">Store Location</h3>
                             <p class="g_aspr text-white">A-103 Designers Park , Plot no. B9/1A ,Sector 62 Noida District G.B. Nagar UP 201301  India .</p>
                             <a href="" class="call_p"><i class="fa fa-phone"></i> +91 9997141786 </a>
                              <div class="media-list_top">
						 
						 
							<ul class="social-nav model-2" style=" ">
								<?php
								if ($facebook != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $facebook;?>" class="facebook social_a" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<?php
								} if ($twitter != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $twitter;?>" class="twitter social_a" target="_blank"><i class="fa fa-twitter"></i></a></li>
							
								<?php
								} if ($pinterest != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $pinterest;?>" class="pinterest social_a" target="_blank"><i class="fa fa-instagram"></i></a></li>
								<?php
								} if ($youtube != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $youtube;?>" class="youtube social_a" target="_blank"><i class="fa fa-youtube"></i></a></li>
							
								<?php
								}
								?>
							</ul>
						</div>
						<!--<a href="<?php //echo base_url(); ?>">-->
      <!--                    	<img class="img-responsive" src="<?php //echo $this->crud_model->logo('home_bottom_logo'); ?>" alt="">-->
						<!--</a>-->
						<p><?php echo $footer_text ;?></p>
						<?php
							echo form_open(base_url() . 'home/subscribe', array(
								'class' => '',
								'method' => 'post'
							));
						?>    
							<!--<div class="form-group row">-->
       <!--                     	<div class="col-md-10">-->
							<!--		<input type="text" class="form-control col-md-8" name="email" id="subscr" placeholder="<?php echo translate('email_address'); ?>">-->
       <!--                         	<span class="btn btn-subcribe subscriber enterer"><?php echo translate('subscribe'); ?> </span>-->
       <!--                         </div>-->
       <!--                         <div class="col-md-10 marketplace mb-2">-->
							<!--		<div class="col-md-12 mb-2"><h4 class="text-primary">Our Market Place </h4></div>-->
									
							<!--		<a href="https://www.meesho.com/search?q=calix%20herbal&searchType=manual&searchIdentifier=text_search" target="_blank"><img src="<?=base_url()?>template/img/meeso.jpg" alt="" style="height:50px;width:100%"></a>-->
							<!--		<a href="https://www.jiomart.com/search/calixherbal" target="_blank"><img src="<?=base_url()?>template/img/jiomart.png" alt="" style="height:50px;width:100%"></a>-->
							<!--		<a href="https://www.amazon.in/stores/CALIX+HERBAL/page/BEA16C2E-A7E4-47B7-B80D-C4AA1287DA1D" target="_bank"><img src="<?=base_url()?>template/img/amazon.jpg" alt="" style="height:50px;width:100%"></a>-->
							<!--		<a href="https://www.flipkart.com/search?q=calix%20herbal&otracker=search&otracker1=search&marketplace=FLIPKART&as-show=on&as=off"  target="_blank"><img src="<?=base_url()?>template/img/flipkart.png" alt="" style="height:35px;width:100%"></a>-->
							<!--	</div>-->
							<!--</div>                -->
					   </form> 
					</div>
				</div>
				<div class="col-md-3 hidden-xs hidden-sm">
					<div class="widget widget-categories">
						<h4 class="widget-title"><?php echo translate('Our Policy');?></h4>
						<ul class="max_height_300">
						 <?php
							$this->db->where('status','ok');
                            $all_page = $this->db->get('page')->result_array();
							foreach($all_page as $row){
							?>
                            <li>
                                <a href="<?php echo base_url(); ?>home/page/<?php echo $row['parmalink']; ?>">
                                    <?php echo $row['page_name']; ?>
                                </a>
                            </li>
                            <?php
							}
							?>
						</ul>
					</div>
				</div>
				<div class="col-md-3  col-sm-12 hidden-xs">
					<div class="widget widget-categories">
						<h4 class="widget-title"><?php echo translate('useful_links');?></h4>
						<div class="max_height_300">
						
						<ul>
							<li>
								<a href="<?php echo base_url(); ?>home/"><?php echo translate('home');?>
								</a>
							</li>
							<!--<li>-->
							<!--	<a href="<?php //echo base_url(); ?>home/category/0/0-0"><?php //echo translate('all_products');?>-->
							<!--	</a>-->
							<!--</li>-->
							<li>
								<a href="<?php echo base_url(); ?>home/others_product/featured"><?php echo translate('featured_products');?>
								</a>
							</li>
							<li>
								<a href="<?php echo base_url(); ?>home/contact/"><?php echo translate('contact');?>
								</a>
							</li>
                      
								<div class="top-bar-right bt_login">
                                ----- ----- -----
                            </div>
						</ul>
						
						</div>
					</div>
				</div>
					<div class="col-md-3  ">
					    	<div class="widget widget-categories">
						<h4 class="widget-title">Newsletter</h4>
												<div class="form-group row">
                            	<div class="col-md-12">
                            	  <p class="g_aspr text-white">Get now free 20% discount for all products on your first order!</p>
									<input type="text" class="form-control col-md-12" name="email" id="subscr" placeholder="<?php echo translate('email_address'); ?>">
                                	<span class="btn btn-subcribe subscriber enterer"><?php echo translate('subscribe'); ?> </span>
                                </div>
                               
							</div>                
						</div>
					</div>
			

			</div>
			<div class="row">
			      <div class="col-md-12">
			          <div class="bootam_footer">
			              <div class="media-list d-none">
						 
						 
							<ul class="social-nav model-2" style=" ">
								<?php
								if ($facebook != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $facebook;?>" class="facebook social_a" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<?php
								} if ($twitter != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $twitter;?>" class="twitter social_a" target="_blank"><i class="fa fa-twitter"></i></a></li>
							
								<?php
								} if ($pinterest != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $pinterest;?>" class="pinterest social_a" target="_blank"><i class="fa fa-instagram"></i></a></li>
								<?php
								} if ($youtube != '') {
								?>
								<li style="border-top: none;"><a href="<?php echo $youtube;?>" class="youtube social_a" target="_blank"><i class="fa fa-youtube"></i></a></li>
							
								<?php
								}
								?>
							</ul>
						</div>
						<!----end--->
						<div class="l_go">	<img class="img-responsive" src="<?php echo $this->crud_model->logo('home_bottom_logo'); ?>" alt=""></div>
							<!----end--->
			    <div class="r_ghtit">	<img src="<?php echo base_url(); ?>uploads/others/payment.png"></div>
			          </div>
			      </div>
			    
			 
			</div>
		</div>
	</div>
	<div class="footer1-meta">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12">
					<div class="copyright text-center" version="Currently <?= demo()?'demo':''?> v<?php echo $this->db->get_where('general_settings',array('type'=>'version'))->row()->value; ?>">
						<?php echo date('Y'); ?> &copy; 
						<?php echo translate('all_rights_reserved'); ?> @ 
						<a href="<?php echo base_url(); ?>">
							<?php echo $system_title; ?>
						</a> 
							| 
						<a href="<?php echo base_url(); ?>home/legal/terms_conditions" class="link">
							<?php echo translate('terms_&_condition'); ?>
						</a> 
							| 
						<a href="<?php echo base_url(); ?>home/legal/privacy_policy" class="link">
							<?php echo translate('privacy_policy'); ?>
						</a>
						
					</div>
				</div>
				<!--<div class="col-md-4 hidden-xs hidden-sm">-->
				<!--	<div class="payments" style="font-size: 30px;">-->
				<!--		<ul>-->
				<!--			<li><img src="<?php //echo base_url(); ?>uploads/others/payment.png"></li>-->
				<!--		</ul>-->
				<!--	</div>-->
				<!--</div>-->
			</div>
		</div>
	</div>
</footer>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Track your Order</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
        	<label>Enter Order Id</label>
        	<input type="text" name="shipment_id" id="shipment_id" class="form-control" required>
        </div>

        <div id="track_result">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="trackBtn">Submit</button>
      </div>
    </div>
  </div>
</div>
<style>
.link:hover{
	text-decoration:underline;
}
.model-2 a {
	margin: 0px 1px;
	height: 32px;
	width: 32px;
	line-height: 32px;

}
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('.set_langs').on('click',function(){
            var lang_url = $(this).data('href');                                    
            $.ajax({url: lang_url, success: function(result){
                location.reload();
            }});
        });
        $('.top-bar-right').load('<?php echo base_url(); ?>home/top_bar_right');
    });
</script>
<script>
    // vars
'use strict'
var	testim = document.getElementById("testim"),
		testimDots = Array.prototype.slice.call(document.getElementById("testim-dots").children),
    testimContent = Array.prototype.slice.call(document.getElementById("testim-content").children),
    testimLeftArrow = document.getElementById("left-arrow"),
    testimRightArrow = document.getElementById("right-arrow"),
    testimSpeed = 4500,
    currentSlide = 0,
    currentActive = 0,
    testimTimer,
		touchStartPos,
		touchEndPos,
		touchPosDiff,
		ignoreTouch = 30;
;

window.onload = function() {

    // Testim Script
    function playSlide(slide) {
        for (var k = 0; k < testimDots.length; k++) {
            testimContent[k].classList.remove("active");
            testimContent[k].classList.remove("inactive");
            testimDots[k].classList.remove("active");
        }

        if (slide < 0) {
            slide = currentSlide = testimContent.length-1;
        }

        if (slide > testimContent.length - 1) {
            slide = currentSlide = 0;
        }

        if (currentActive != currentSlide) {
            testimContent[currentActive].classList.add("inactive");            
        }
        testimContent[slide].classList.add("active");
        testimDots[slide].classList.add("active");

        currentActive = currentSlide;
    
        clearTimeout(testimTimer);
        testimTimer = setTimeout(function() {
            playSlide(currentSlide += 1);
        }, testimSpeed)
    }

    testimLeftArrow.addEventListener("click", function() {
        playSlide(currentSlide -= 1);
    })

    testimRightArrow.addEventListener("click", function() {
        playSlide(currentSlide += 1);
    })    

    for (var l = 0; l < testimDots.length; l++) {
        testimDots[l].addEventListener("click", function() {
            playSlide(currentSlide = testimDots.indexOf(this));
        })
    }

    playSlide(currentSlide);

    // keyboard shortcuts
    document.addEventListener("keyup", function(e) {
        switch (e.keyCode) {
            case 37:
                testimLeftArrow.click();
                break;
                
            case 39:
                testimRightArrow.click();
                break;

            case 39:
                testimRightArrow.click();
                break;

            default:
                break;
        }
    })
		
		testim.addEventListener("touchstart", function(e) {
				touchStartPos = e.changedTouches[0].clientX;
		})
	
		testim.addEventListener("touchend", function(e) {
				touchEndPos = e.changedTouches[0].clientX;
			
				touchPosDiff = touchStartPos - touchEndPos;
			
				console.log(touchPosDiff);
				console.log(touchStartPos);	
				console.log(touchEndPos);	

			
				if (touchPosDiff > 0 + ignoreTouch) {
						testimLeftArrow.click();
				} else if (touchPosDiff < 0 - ignoreTouch) {
						testimRightArrow.click();
				} else {
					return;
				}
			
		})
}
// MDB Lightbox Init
$(document).on("click", '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
</script>

<script>

$('#trackBtn').click(function(){
	var shipment_id = $('#shipment_id').val();
	var url = '<?=base_url('home/trackMyOrder');?>'
	if(shipment_id=='')
	{
		$('#track_result').html('');
		$('#track_result').html('Please enter Shipment id').fadeIn();
		return false;
	}else{
		$('#track_result').html('');
	}
	// alert(shipment_id);
	// alert('good');
	$.ajax({
		url: url, // form action url
		cache: false,
    	dataType: "html",
    	data: {ship_id: shipment_id},
		beforeSend: function() {
		},
		success: function(data) {
			if(data !== ''){
				$('#track_result').html('');
				$('#track_result').html(data).fadeIn(); // fade in response data
			}
		},
		error: function(e) {
			console.log(e)
		}
	});
});
</script>
<script>
		$('#category-products-carousel').owlCarousel({
			autoplay: true,
			autoplayHoverPause: true,
			loop:true,
			margin: 30,
			dots: false,
			nav: true,
			navText: [
				"<i class='fa fa-angle-left'></i>",
				"<i class='fa fa-angle-right'></i>"
			],
			responsive: {
				0: {items: 2},
				479: {items: 3},
				768: {items: 4},
				991: {items: 5},
				1024: {items: 6}
			}
		});
</script>