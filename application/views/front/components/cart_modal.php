<div class="modal fade popup-cart" id="popup-cart" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="container">
            <div class="cart-items">
                <div class="cart-items-inner">
                    <span class="top_carted_list">
                    </span>
                    <div class="offer-part">
                        <p class="pull-right item-price shopping-cart__offer_discount"></p>
                        <div class="media-body">
                            <h4 class="media-heading item-title offer_summary">
                            </h4>
                        </div>
                    </div>
                    <div class="media">
                        <p class="pull-right item-price shopping-cart__total"></p>
                        <div class="media-body">
                            <h4 class="media-heading item-title summary">
                                <?php echo translate('subtotal');?>
                            </h4>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-body">
                            <div>
                                <span class="btn  btn-theme-transparent" data-dismiss="modal">
                                    <?php echo translate('close');?>
                                </span><!--
                                -->
                                <a href="<?php echo base_url(); ?>home/cart_checkout" class="btn  btn-theme-dark btn-call-checkout">
                                    <?php echo translate('checkout');?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>