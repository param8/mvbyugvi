<?php 
//echo $product_id;die;
// $this->db->where('id',$product_id);
// $product = $this->db->get('product')->row();
// print_r($product);die;
 
?>
<div class="thumbnail box-style-1 no-paddings" itemscope itemtype="http://schema.org/Product">
    <div class="media">
    	<!--<div class="cover"></div>-->
    	<a itemprop="url" href="<?=$this->crud_model->product_link($product_id);?>"><?php //echo $this->crud_model->product_link($product_id); ?>
        <div class="media-link image_delay" data-src="<?php echo $this->crud_model->file_view('product',$product_id,'','','thumb','src','multi','one'); ?>" style="background-image:url('<?php echo img_loading(); ?>');background-size:cover;">
        	<?php
                if($this->crud_model->get_type_name_by_id('product',$product_id,'current_stock') <=0 && !$this->crud_model->is_digital($product_id)){ 
            ?>
                <div class="sticker red">
                    <?php echo translate('out_of_stock'); ?>
                </div>
            <?php
                }
            ?>
            <?php 
                if (null == $this->session->userdata('currency') || $this->session->userdata('currency')==1) {
                    $discount = 'discount';   
                }else{$discount = 'discount_usd';}        
                $discount= $this->db->get_where('product',array('product_id'=>$product_id))->row()->$discount ;           
                if($discount > 0){ 
            ?>
            <div class="sticker green">
                <?php echo translate('discount');?> 
				<?php 
                    if (null == $this->session->userdata('currency') || $this->session->userdata('currency')==1) {
                    $discount_type = 'discount_type';   
                    }else{$discount_type = 'discount_type_usd';}    
                     $type = $this->db->get_where('product',array('product_id'=>$product_id))->row()->$discount_type ; 
                     if($type =='amount'){
                          echo currency($discount); 
                          } else if($type == 'percent'){
                               echo $discount; 
                ?> 
                    % 
                <?php 
                    }
                ?>
            </div>
            <?php } ?>
            <?php
                $offer_id = isOfferApplicable($product_id);
                if($offer_id > 0){
                    $offer_title= $this->db->get_where('offer',array('offer_id'=>$offer_id))->row()->offer_title;
            ?>
            <div class="offer-sticker orange">
                <?php echo $offer_title; ?>
            </div>
            <?php } ?>
            <div class="quick-view-sm hidden-xs hidden-sm">
                <!--<span onclick="quick_view('<?php //echo $this->crud_model->product_link($product_id,'quick'); ?>')">-->
                <!--    <span class="icon-view" data-toggle="tooltip" data-original-title="<?php  //echo translate('quick_view'); ?>">-->
                       
                <!--        <strong><i class="fa fa-eye"></i></strong>-->
                <!--    </span>-->
                <!--</span>-->
                <div class="view_btn"> 
                     <div class="button">
                            <span class="icon-view1  " onclick="do_compare(<?php echo $product_id; ?>,event)" data-toggle="tooltip" 
                            	data-original-title="<?php if($this->crud_model->is_compared($product_id)=="yes"){ echo translate('compared'); } else { echo translate('compare'); } ?>">
                                <strong><i class="fa fa-exchange"></i></strong>
                            </span>
                            <span class="icon-view1  " onclick="to_wishlist(<?php echo $product_id; ?>,event)" data-toggle="tooltip" 
                            	data-original-title="<?php if($this->crud_model->is_wished($product_id)=="yes"){ echo translate('added_to_wishlist'); } else { echo translate('add_to_wishlist'); } ?>">
                                <strong><i class="fa fa-heart"></i></strong>
                            </span>
                            <span class="icon-view1   " onclick="to_cart(<?php echo $product_id; ?>,event)" data-toggle="tooltip" 
                            	data-original-title="<?php if($this->crud_model->is_added_to_cart($product_id)){ echo translate('added_to_cart'); } else { echo translate('add_to_cart'); } ?>">
                                <strong><i class="fa fa-shopping-cart"></i></strong>
                            </span>
                     </div>
                </div>
                
            </div>
        </div>
        </a>
    </div>
    <div class="caption text-center" data-toggle="tooltip" title="<?php echo $title; ?>">
        <h4 itemprop="name" class="caption-title" >
        	<a itemprop="url" href="<?php echo $this->crud_model->product_link($product_id); ?>">
				<span itemprop="name" ><?php echo $title; ?></span>
            </a>
        </h4>
        <?php if($this->crud_model->get_type_name_by_id('ui_settings','66','value') == 'ok'){
        ?>
        <?php $rating = $this->crud_model->rating($product_id); ?>
        <?php $rating_count = $this->crud_model->rating_count($product_id); ?>
        <span style="display: none" itemprop="ratingValue"><?= $rating?></span>
        <span style="display: none" itemprop="reviewCount"><?= $rating_count?></span>
        <div class="rateit" data-rateit-value="<?= $rating ?>" data-rateit-ispreset="true" data-rateit-readonly="true" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating"></div>
        <?php }else{ ?>
        <?php 
              $min = 4.5;
              $max = 5;
              $rand_rating = mt_rand($min*10, $max*10) / 10; 
        ?>
        <span style="display: none" itemprop="ratingValue"><?=$rand_rating;?></span>
        <div class="rateit" data-rateit-value="<?=$rand_rating;?>" data-rateit-ispreset="true" data-rateit-readonly="true" itemprop="aggregateRating" itemscope itemtype="https://schema.org/AggregateRating"></div>
        <?php } ?>
        <?php
        if (null == $this->session->userdata('currency') || $this->session->userdata('currency')==1) {
        ?>
        <div class="price">
            <?php if($this->crud_model->get_type_name_by_id('product',$product_id,'discount') > 0){ ?> 
                <ins><?php echo currency($this->crud_model->get_product_price($product_id)); ?> </ins> 
                <del itemprop="price"><?php echo currency($sale_price); ?></del>
            <?php } else { ?>
                <ins itemprop="price"><?php echo currency($sale_price); ?></ins>
            <?php }?>
        </div>
        <?php }else{ ?>
        <div class="price">
            <?php if($this->crud_model->get_type_name_by_id('product',$product_id,'discount_usd') > 0){ ?> 
                <ins><?php echo currency($this->crud_model->get_product_price($product_id)); ?> </ins> 
                <del itemprop="price"><?php echo currency($sale_price_usd); ?></del>
            <?php } else { ?>
                <ins itemprop="price"><?php echo currency($sale_price_usd); ?></ins>
            <?php }?>
        </div>
        <?php } ?>
        <?php if ($this->db->get_where('general_settings', array('general_settings_id' => '58'))->row()->value == 'ok' and $this->db->get_where('general_settings', array('general_settings_id' => '81'))->row()->value == 'ok'): ?>
        <div class="vendor">
            <?php echo $this->crud_model->product_by($product_id,'with_link'); ?>
        </div>
        <?php endif ?>
      
    </div>
</div>