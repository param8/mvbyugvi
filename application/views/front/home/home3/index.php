 <!--<div class="banner_top">-->
 <!--     <div class="container">-->
 <!--    <div class="ser-dsc" >-->
	<!--		 <h3>Shop and Save Big on Hottest Products</h3>-->
 <!--           <div class="offtxt text-right">-->
 <!--           	<p><strong>From</strong><b>$80</b><br>Don't miss this special opportunity today.</p>-->
 <!--           	<a href="#" class="btn btn-primary">shop now</a>-->
 <!--           </div>-->



	<!--		</div>-->
 <!--</div>-->
 <!--</div>-->
<?php 
	if($this->crud_model->get_type_name_by_id('general_settings','62','value') == 'ok'){
		include 'category_menu.php';
	}
?>
 
 <!---Delivery---->
 <section class="py-5 mt-5" style="padding-bottom:0 !Important;">
     
<div class="container">
  <div class="row deliveryinfo text-xs-center">
    <div class="col-sm-3 col-xs-12 sbr">
      <ul class="list-unstyled">
        <li><img src="<?php echo base_url();?>/template/front/img/d1.png"></span></li>
        <li class="text-xs-left">
          <h4>Free Shiping</h4>
          <p>On Order $23 - 7 Days A Week</p>
        </li>
      </ul>
    </div>
    <div class="col-sm-3 col-xs-12 sbr">
      <ul class="list-unstyled">
        <li><img src="<?php echo base_url();?>/template/front/img/d2.png"></span></li>
        
        <li class="text-xs-left">
          <h4>Money Back Guarantee</h4>
          <p>Send Within 30 days</p>
        </li>
      </ul>
    </div>
    <div class="col-sm-3 col-xs-12 sbr">
      <ul class="list-unstyled">
         <li><img src="<?php echo base_url();?>/template/front/img/d3.png"></span></li>
        <li class="text-xs-left">
          <h4>Free Returns</h4>
          <p>free 90 days returns policy</p>
        </li>
      </ul>
    </div>
    <div class="col-sm-3 col-xs-12 sbr">
      <ul class="list-unstyled">
      <li><img src="<?php echo base_url();?>/template/front/img/d4.png"></span></li>
        <li class="text-xs-left">
          <h4>24/7 Customer Service</h4>
          <p>Call us 24/7 at 000 -123 - 455</p>
        </li>
      </ul>
    </div>
  </div>
</div>
 </section>
 
<!----Delivery---->
<section class="Picks ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
               <div class="top_h">
                    <h2 class="section-title section-title-lg section-title-2 mr_0"> <span> Our Picks for You  </span> </h2>
        	       <p class="m-0 text_cenetr">Our products are designed for everyone.</p>
               </div>
            </div>
        </div>
    <div class="carousel-arrow-alt">
            <div class="owl-carousel carousel-arrow" id="category-products-carousel">
    <?php
            if(count($categories)>0){
              foreach($categories as $category){
              ?>

            
                <div class="product-box-4" id="product-box-6" itemscope itemtype="http://schema.org/Product">
                    <div class="img">
                        <a href="<?=base_url('category/'.$category['slug']);?>" class="d-block hover-zoom-in rounded-circle">
                            <img src="<?=base_url('uploads/category_image/'.$category['banner']);?>" alt="<?=strtolower(str_replace(' ', '-',$category['category_name']));?>" class="image_delay" data-src="<?=base_url('uploads/category_image/'.$category['banner']);?>">
                            <h4 class="text-center fs-20"><a href="<?=base_url('category/'.$category['slug']);?>" class="text-decoration-none"><?=$category['category_name'];?></h4>
                        </a>
                    </div>
                </div>
                
            <?php }}?>
            </div>
                </div>
    </div>
    
<!------category------->
<?php 
	include 'top_banner.php';
?>
<?php
	if($this->crud_model->get_type_name_by_id('ui_settings','59','value') == 'ok'){
		include 'todays_deal.php';
	}
?>


<?php
	if($this->crud_model->get_type_name_by_id('ui_settings','24','value') == 'ok'){
		include 'featured_products.php';
	}
?>
<?php
	if ($this->crud_model->get_type_name_by_id('general_settings','82','value') == 'ok') {
		if($this->crud_model->get_type_name_by_id('ui_settings','40','value') == 'ok'){
			include 'product_bundle.php';
		}
	}
	if($this->crud_model->get_type_name_by_id('general_settings','83','value') == 'ok'){
		if($this->crud_model->get_type_name_by_id('ui_settings','43','value') == 'ok'){
			include 'customer_products.php';
		}
	}
?>
<!---Save on Sets---->
<?php
    $today = date("Y-m-d H:i:s");
    $spl_offer = $this->db->where('is_special_offer', 1)
                      ->where("offer_start_date_time <=",$today)
                      ->where("offer_end_date_time >=",$today)
                      ->get("product");
    $offer_res = $spl_offer->row_array();                  
    // var_dump($offer_res);
    if($offer_res){
?>
<section class="pt-xl-15 pt-11 pb-lg-11 pb-0 s_sets" data-animated-id="5">
<div class="container container-xl">
<div class="row align-items-center">
<div class="col-lg-6">
<img src="<?php echo base_url('uploads/offer_image/'.$offer_res['offer_image']); ?>" alt="Countdown-02" style="width:100%">
</div>
<div class="offset-xl-1 col-xl-5 col-lg-6 py-lg-0 py-9">
    <div class="r_side">
<div class="d-flex align-items-center fs-15 text-secondary text-uppercase font-weight-600 letter-spacing-01 mb-4 off_sp">Special offer<span class="badge badge-primary fs-15 font-weight-500 py-1 px-2 ml-2 ">
-
<?php 
    if (null == $this->session->userdata('currency') || $this->session->userdata('currency')==1) {
        $discount = 'discount';   
    }else{$discount = 'discount_usd';}        
    $discount= $this->db->get_where('product',array('product_id'=>$offer_res['product_id']))->row()->$discount ;           
    if($discount > 0){ 
        if (null == $this->session->userdata('currency') || $this->session->userdata('currency')==1) {
        $discount_type = 'discount_type';   
        }else{$discount_type = 'discount_type_usd';}    
         $type = $this->db->get_where('product',array('product_id'=>$offer_res['product_id']))->row()->$discount_type ; 
         if($type =='amount'){
              echo currency($discount); 
              } else if($type == 'percent'){
                   echo $discount .'%';
                }
    }         
?>      
</span></div>
<?php echo $offer_res['special_offer_details']; ?>
<div class="countdown d-flex mb-7 mx-n2 mx-sm-n4" data-countdown="true" data-countdown-end="Jan 27, 2023 18:24:25">
<div class="countdown-item center px-2 px-sm-4">
<span class="fs-40 fs-sm-48 lh-1 text-primary font-weight-600 day" id="c_days">-14</span>
</div>
<div class="separate fs-30">:</div>
<div class="countdown-item text-center px-2 px-sm-4">
<span class="fs-40 fs-sm-48 lh-1 text-primary font-weight-600 hour" id="c_hour">16</span>
</div>
<div class="separate fs-30">:</div>
<div class="countdown-item text-center px-2 px-sm-4">
<span class="fs-40 fs-sm-48 lh-1 text-primary font-weight-600 minute" id="c_min">58</span>
</div>
<div class="separate fs-30">:</div>
<div class="countdown-item text-center px-2 px-sm-4">
<span class="fs-40 fs-sm-48 lh-1 text-primary font-weight-600 second" id="c_sec">22</span>
</div>
</div>
<a href="<?php echo $this->crud_model->product_link($offer_res['product_id']); ?>" class="btn btn-secondary bg-hover-primary border-hover-primary get">
Get Only
    <?php
        if (null == $this->session->userdata('currency') || $this->session->userdata('currency')==1) {
         if($this->crud_model->get_type_name_by_id('product',$offer_res['product_id'],'discount') > 0){ 
            echo currency($this->crud_model->get_product_price($offer_res['product_id'])); 
            } else { 
                echo currency($sale_price); 
             }
        }else{ 

            if($this->crud_model->get_type_name_by_id('product',$offer_res['product_id'],'discount_usd') > 0){ 
             echo currency($this->crud_model->get_product_price($offer_res['product_id'])); 
            } else { 
             echo currency($sale_price_usd);
            }
        } 
    ?>
</a>
</div>
</div>
</div>
</div>
<p id="demo"></p>
</section>
<script>
    // Set the date we're counting down to
    // 1. JavaScript
    // var countDownDate = new Date("Sep 5, 2018 15:37:25").getTime();
    // 2. PHP
    var countDownDate = <?php echo strtotime($offer_res['offer_end_date_time']) ?> * 1000;
    var now = <?php echo time() ?> * 1000;

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        // 1. JavaScript
        // var now = new Date().getTime();
        // 2. PHP
        now = now + 1000;

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("c_days").innerHTML = days+ "d ";
        document.getElementById("c_hour").innerHTML = hours+ "h ";
        document.getElementById("c_min").innerHTML = minutes+ "m ";
        document.getElementById("c_sec").innerHTML = seconds+ "s ";

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
    </script>
<?php } ?>
<!---Save on Sets---->
<?php 
	if ($this->crud_model->get_type_name_by_id('general_settings','58','value') == 'ok') {
		if($this->crud_model->get_type_name_by_id('ui_settings','25','value') == 'ok'){
			include 'vendors.php';
		}
	}
?>
<?php 
	include 'category_products.php';
?>

<?php
    $discover_now=$this->db->get('discover')->row_array();
    if($discover_now){
?>
<section class="video">
    <div class="container">
    <div class="Sec_part">
        <div class="contect c_f">
             <?=$discover_now['details'];?>
<button class="btn btn-secondary bg-hover-primary border-hover-primary get" onclick="window.location.href='<?=$discover_now['discover_link']?>'">Discover Now</button>
        </div>
         <div class="contect r_ct">
             <img src="<?=base_url('uploads/discover/'.$discover_now['filename'])?>" style="
    width: 100%;
">
              <button id="play-video" type="button" class="btn  video-btn video-play-button" data-toggle="modal"
    data-src="<?=$discover_now['discover_video_url'];?>" data-target="#myModal" style="display:none;">
    <span></span>
   
   
  </button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-body">

          <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
          <!--  <span aria-hidden="true">&times;</span>-->
          <!--</button>-->
          <!-- 16:9 aspect ratio -->
          <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="<?=$discover_now['discover_video_url'];?>" id="video"
              allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>

        </div>

      </div>
    </div>
  </div>
  <!---end-modal--->
 
         </div>
    </div>
      </div>
       </section>
     <script>
      $(document).ready(function() {
  // Gets the video src from the data-src on each button
  var $videoSrc;
  $(".video-btn").click(function() {
    $videoSrc = $(this).attr("data-src");
    console.log("button clicked" + $videoSrc);
  });

  // when the modal is opened autoplay it
  $("#myModal").on("shown.bs.modal", function(e) {
    console.log("modal opened" + $videoSrc);
    // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
    $("#video").attr(
      "src",
      $videoSrc + "?autoplay=1&showinfo=0&modestbranding=1&rel=0&mute=1"
    );
  });

  // stop playing the youtube video when I close the modal
  $("#myModal").on("hide.bs.modal", function(e) {
    // a poor man's stop video
    $("#video").attr("src", $videoSrc);
  });

  // document ready
});
  </script>
</section>
<?php } ?>

<?php 
	if($this->crud_model->get_type_name_by_id('ui_settings','26','value') == 'ok'){
		include 'blog.php';
	}
?>

<?php
    if($this->crud_model->get_type_name_by_id('ui_settings','65','value') == 'ok'){ 
        include 'media_presence.php';
    }
?>

<?php
    if($this->crud_model->get_type_name_by_id('ui_settings','64','value') == 'ok'){ 
        include 'testimonials.php';
    }
?>


<?php
	if($this->crud_model->get_type_name_by_id('ui_settings','31','value') == 'ok'){
		include 'special_products.php';
	}
?>
<?php 
	if ($this->crud_model->get_type_name_by_id('general_settings','68','value') == 'ok') {
		if($this->crud_model->get_type_name_by_id('ui_settings','23','value') == 'ok'){
			include 'brands.php';
		}
	}
?>

