<section class="page-section featured-products sl-featured mediaprsc">
  <div class="container">
    <h2 class="section-title section-title-lg section-title-2">
      <span> Media Presence </span>
    </h2>
    <div class="row d-flex flex-wrap align-items-center" data-toggle="modal" data-target="#lightbox">
      <?php
          $limit =  8;
          $this->db->limit($limit);
          $this->db->order_by("media_id", "desc");
          $media=$this->db->get('media')->result_array();
          foreach($media as $row){
      ?>

      <div class="col-12 col-md-6 col-lg-3">
        <img src="<?=base_url('uploads/media/'.$row['filename']);?>" />
        <h4 class="text-center mb-4"><a href="<?php echo $row['media_link']; ?>" target="_blank"><?php echo $row['media_title']; ?></a></h4>
      </div>
      <?php
          }
      ?>
    </div>
    <!-- Modal -->
  </div>
</section>