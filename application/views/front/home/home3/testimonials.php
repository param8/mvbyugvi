<section class="page-section featured-products sl-featured mediaprsc">
    <div class="container">
        <h2 class="section-title section-title-lg section-title-2">
            <span>Testimonial</span>
        </h2>
         <div class="partners-carousel">
            <div class="brand-carousel carousel-arrow">
                <?php
                    $limit =  8;
                    $this->db->limit($limit);
                    $this->db->order_by("testimonial_id", "desc");
                    $media=$this->db->get('testimonials')->result_array();
                    foreach($media as $row){
                ?>
                <div class="p-item">
                        <img class="image_delay" src="<?php echo $this->crud_model->file_view('testimonial',$row['testimonial_id'],'','','thumb','src','',''); ?>"  alt="testimonial"/> 
                        <div class="texttesti">
                            <p><?php echo $row['description'];?></p>
                            <h4><?php echo $row['title'];?></h4>
                        </div>
                </div>
                <?php } ?>
            </div>
        </div>

    </div>
</section>

<script>
    $(document).ready(function(){
    $(".brand-carousel").owlCarousel({
        autoplay: false,
        autoplayHoverPause: true,
        loop: true,
        margin: 20,
        dots: false,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
        responsive: {
            0: {items: 2},
            479: {items: 2},
            768: {items: 2},
            991: {items: 3},
            1024: {items: 3},
            1280: {items: 3}
        }
    });
});

</script>