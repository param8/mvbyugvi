<!-- PAGE -->
<?php
    $this->db->order_by("media_id", "desc");
    $offer_banners=$this->db->get('media')->result_array();
    $i=1;

    $count=count($offer_banners);
    if($count==1){
        $md=12;
        $sm=12;
        $xs=12;
    }elseif($count==2){
        $md=6;
        $sm=6;
        $xs=6;
    }elseif($count==3){
        $md=4;
        $sm=4;
        $xs=12;
    }
    elseif($count==4){
        $md=3;
        $sm=6;
        $xs=6;
    }
    
    if($count!==0){
?>
<section class="page-section tttt" style="padding-top: 0px">
    <div class="container">
        <div class="row">
            <?php
            //print_r($offer_banners);
            foreach($offer_banners as $row){
            ?>
            <div class="col-md-<?php echo $md; ?> col-sm-<?php echo $sm; ?> col-xs-<?php echo $xs; ?>">
           
                <div class="thumbnail  my-2 no-scale no-border no-padding thumbnail-banner size-1x<?php echo $count; ?>">
                    <div class="media">
                       
                        <a class="media-link" href="<?php echo $row['media_link']; ?>">
                            <img src="<?php echo base_url('uploads/media/'.$row['filename']); ?>" style=" height: 340px;">
                            <div class="layer"> </div>
                        </a>
                    </div>
                    <div class="neww">
                        <h1 class="card-title fs-34 text-white mb-3"><?=$row['media_title'];?></h1>
                        <div>
                        <a href="<?=$row['media_link'];?>" class="btn btn-link btn-light bg-transparent text-white hover-white border-0 p-0 fs-16 font-weight-600">
                        Discover Now <i class="fa fa-arrow-right"></i>
                        </a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }
            ?>
        </div>
    </div>
</section>
<?php
    }
?>
<!-- /PAGE -->