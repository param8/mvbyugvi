<?php

	function isOfferApplicable($product_id)
	{	
		$CI=& get_instance();
		$CI->load->database();

		$CI->db->where('offer_status', 'ok');
		$CI->db->where('offer_start_date_time <=', date('Y-m-d H:i:s'));
        $CI->db->where('offer_end_date_time >=', date('Y-m-d H:i:s'));
		$CI->db->like('offer_on_pro', $product_id);
		$query = $CI->db->get('offer');
		
		if($query->num_rows() > 0)
		{
			$row = $query->row_array();
			return $row['offer_id']; 
		}else{
			return 0;
		}
		// echo $CI->db->last_query();
	}

?>